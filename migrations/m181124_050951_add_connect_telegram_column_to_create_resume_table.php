<?php

use yii\db\Migration;

/**
 * Handles adding connect_telegram to table `create_resume`.
 */
class m181124_050951_add_connect_telegram_column_to_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('create_resume', 'connect_telegram', $this->boolean());
        $this->addColumn('create_resume', 'date_cr', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('create_resume', 'connect_telegram');
        $this->dropColumn('create_resume', 'date_cr');
    }
}
