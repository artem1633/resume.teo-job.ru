<?php

use yii\db\Migration;

/**
 * Handles the creation of table `education`.
 */
class m181113_075133_create_education_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('education', [
            'id' => $this->primaryKey(),
            'educational_institution' => $this->string(255),
            'faculty' => $this->string(255),
            'specialty' => $this->integer(),
            'create_id' => $this->integer(),
            'form_study' => $this->string(255),
            'year_ending' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('education');
    }
}
