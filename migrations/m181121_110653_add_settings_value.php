<?php

use yii\db\Migration;

/**
 * Class m181121_110653_add_settings_value
 */
class m181121_110653_add_settings_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {   
        $this->insert('settings',array(
            'name' => 'Текст после заполнения анкеты',
            'key' => 'text_after_filling',
            'value' =>'Для того чтобы получать уведомления вступите в телеграм бот @teo_job_info_bot и введите код "{unique_code_for_telegram}"',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181121_110653_add_settings_value cannot be reverted.\n";

        return false;
    }
    */
}
