<?php

use yii\db\Migration;

/**
 * Handles the creation of table `create_resume`.
 */
class m181113_073923_create_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('create_resume', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(255),
            'name' => $this->string(255),
            'middle_name' => $this->string(255),
            'position' => $this->string(255),
            'travel' => $this->boolean(),
            'desired_salary' => $this->integer(),
            'email' => $this->string(255),
            'phone' => $this->string(255),
            'viber' => $this->boolean(),
            'whatsapp' => $this->boolean(),
            'telegram' => $this->boolean(),
            'image' => $this->string(255),
            'ssilka' => $this->string(255),
            'employment' => $this->integer(),
            'schedule' => $this->integer(),
            'city_id' => $this->integer(),
            'citizenship' => $this->string(255),
            'birthday' => $this->date(),
            'moving' => $this->integer(),
            'floor' => $this->integer(),
            'familiy_status' => $this->integer(),
            'have_kids' => $this->boolean(),
            'languages_id' => $this->text(),
            'prava_m' => $this->boolean(),
            'prava_a' => $this->boolean(),
            'prava_b' => $this->boolean(),
            'prava_c' => $this->boolean(),
            'prava_d' => $this->boolean(),
            'prava_tm' => $this->boolean(),
            'prava_tv' => $this->boolean(),
            'medical_book' => $this->boolean(),
            'information' => $this->text(),
            'offers' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('create_resume');
    }
}
