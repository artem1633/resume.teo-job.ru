<?php

use yii\db\Migration;

/**
 * Class m181120_110732_add_city_value
 */
class m181120_110732_add_city_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('city',array(
            'name' => 'Абакан',          
        ));
        $this->insert('city',array(
            'name' => 'Азов',          
        ));
        $this->insert('city',array(
            'name' => 'Александров',          
        ));
        $this->insert('city',array(
            'name' => 'Алексин',          
        ));
        $this->insert('city',array(
            'name' => 'Альметьевск',          
        ));
        $this->insert('city',array(
            'name' => 'Ангарск',          
        ));
        $this->insert('city',array(
            'name' => 'Анжеро-Судженск',          
        ));
        $this->insert('city',array(
            'name' => 'Апатиты',          
        ));
        $this->insert('city',array(
            'name' => 'Арзамас',          
        ));
        $this->insert('city',array(
            'name' => 'Армавир',          
        ));
        $this->insert('city',array(
            'name' => 'Арсеньев',          
        ));
        $this->insert('city',array(
            'name' => 'Артем',          
        ));
        $this->insert('city',array(
            'name' => 'Архангельск',          
        ));
        $this->insert('city',array(
            'name' => 'Асбест',          
        ));
        $this->insert('city',array(
            'name' => 'Астрахань',          
        ));
        $this->insert('city',array(
            'name' => 'Ачинск',          
        ));
        $this->insert('city',array(
            'name' => 'Балаково',          
        ));
        $this->insert('city',array(
            'name' => 'Балахна',          
        ));
        $this->insert('city',array(
            'name' => 'Балашиха',          
        ));
        $this->insert('city',array(
            'name' => 'Балашов',          
        ));
        $this->insert('city',array(
            'name' => 'Барнаул',          
        ));
        $this->insert('city',array(
            'name' => 'Батайск',          
        ));
        $this->insert('city',array(
            'name' => 'Белгород',          
        ));
        $this->insert('city',array(
            'name' => 'Белебей',          
        ));
        $this->insert('city',array(
            'name' => 'Белово',          
        ));
        $this->insert('city',array(
            'name' => 'Белогорск (Амурская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Белорецк',          
        ));
        $this->insert('city',array(
            'name' => 'Белореченск',          
        ));
        $this->insert('city',array(
            'name' => 'Бердск',          
        ));
        $this->insert('city',array(
            'name' => 'Березники',          
        ));
        $this->insert('city',array(
            'name' => 'Березовский (Свердловская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Бийск',          
        ));
        $this->insert('city',array(
            'name' => 'Биробиджан',          
        ));
        $this->insert('city',array(
            'name' => 'Благовещенск (Амурская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Бор',          
        ));

        $this->insert('city',array(
            'name' => 'Борисоглебск',          
        ));$this->insert('city',array(
            'name' => 'Боровичи',          
        ));
        $this->insert('city',array(
            'name' => 'Братск',          
        ));
        $this->insert('city',array(
            'name' => 'Брянск',          
        ));
        $this->insert('city',array(
            'name' => 'Бугульма',          
        ));
        $this->insert('city',array(
            'name' => 'Буденновск',          
        ));
        $this->insert('city',array(
            'name' => 'Бузулук',          
        ));
        $this->insert('city',array(
            'name' => 'Буйнакск',          
        ));
        $this->insert('city',array(
            'name' => 'Великие Луки',          
        ));
        $this->insert('city',array(
            'name' => 'Великий Новгород',          
        ));
        $this->insert('city',array(
            'name' => 'Верхняя Пышма',          
        ));
        $this->insert('city',array(
            'name' => 'Видное',          
        ));
        $this->insert('city',array(
            'name' => 'Владивосток',          
        ));
        $this->insert('city',array(
            'name' => 'Владикавказ',          
        ));
        $this->insert('city',array(
            'name' => 'Волгоград',          
        ));

        $this->insert('city',array(
            'name' => 'Волгодонск',          
        ));
        $this->insert('city',array(
            'name' => 'Волжск',          
        ));
        $this->insert('city',array(
            'name' => '',          
        ));
        $this->insert('city',array(
            'name' => 'Волжский',          
        ));
        $this->insert('city',array(
            'name' => 'Вологда',          
        ));
        $this->insert('city',array(
            'name' => 'Вольск',          
        ));
        $this->insert('city',array(
            'name' => 'Воркута',          
        ));
        $this->insert('city',array(
            'name' => 'Воронеж',          
        ));
        $this->insert('city',array(
            'name' => 'Воскресенск',          
        ));
        $this->insert('city',array(
            'name' => 'Воткинск',          
        ));
        $this->insert('city',array(
            'name' => 'Всеволожск',          
        ));
        $this->insert('city',array(
            'name' => 'Выборг',          
        ));
        $this->insert('city',array(
            'name' => 'Выкса',          
        ));
        $this->insert('city',array(
            'name' => 'Вязьма',          
        ));
        $this->insert('city',array(
            'name' => 'Гатчина',          
        ));
        $this->insert('city',array(
            'name' => 'Геленджик',          
        ));

        $this->insert('city',array(
            'name' => 'Георгиевск',          
        ));
        $this->insert('city',array(
            'name' => 'Глазов',          
        ));
        $this->insert('city',array(
            'name' => 'Горно-Алтайск',          
        ));
        $this->insert('city',array(
            'name' => 'Грозный',          
        ));
        $this->insert('city',array(
            'name' => 'Губкин',          
        ));
        $this->insert('city',array(
            'name' => 'Гудермес',          
        ));
        $this->insert('city',array(
            'name' => 'Гуково',          
        ));
        $this->insert('city',array(
            'name' => 'Гусь-Хрустальный',          
        ));
        $this->insert('city',array(
            'name' => 'Дербент',          
        ));
        $this->insert('city',array(
            'name' => 'Дзержинск',          
        ));
        $this->insert('city',array(
            'name' => 'Димитровград',          
        ));
        $this->insert('city',array(
            'name' => 'Дмитров',          
        ));
        $this->insert('city',array(
            'name' => 'Долгопрудный',          
        ));
        $this->insert('city',array(
            'name' => 'Домодедово',          
        ));
        $this->insert('city',array(
            'name' => 'Донской',          
        ));
        $this->insert('city',array(
            'name' => 'Дубна',          
        ));

        $this->insert('city',array(
            'name' => 'Евпатория',          
        ));
        $this->insert('city',array(
            'name' => 'Егорьевск',          
        ));
        $this->insert('city',array(
            'name' => 'Ейск',          
        ));
        $this->insert('city',array(
            'name' => 'Елабуга',          
        ));
        $this->insert('city',array(
            'name' => 'Елец',          
        ));
        $this->insert('city',array(
            'name' => 'Ессентуки',          
        ));
        $this->insert('city',array(
            'name' => 'Железногорск (Красноярский край)',          
        ));
        $this->insert('city',array(
            'name' => 'Железногорск (Курская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Жигулевск',          
        ));
        $this->insert('city',array(
            'name' => 'Жуковский',          
        ));
        $this->insert('city',array(
            'name' => 'Заречный',          
        ));
        $this->insert('city',array(
            'name' => 'Зеленогорск',          
        ));
        $this->insert('city',array(
            'name' => 'Зеленодольск',          
        ));
        $this->insert('city',array(
            'name' => 'Златоуст',          
        ));
        $this->insert('city',array(
            'name' => 'Иваново',          
        ));

        $this->insert('city',array(
            'name' => 'Ивантеевка',          
        ));$this->insert('city',array(
            'name' => 'Ижевск',          
        ));
        $this->insert('city',array(
            'name' => 'Избербаш',          
        ));
        $this->insert('city',array(
            'name' => 'Иркутск',          
        ));
        $this->insert('city',array(
            'name' => 'Искитим',          
        ));
        $this->insert('city',array(
            'name' => 'Ишим',          
        ));
        $this->insert('city',array(
            'name' => 'Ишимбай',          
        ));
        $this->insert('city',array(
            'name' => 'Йошкар-Ола',          
        ));
        $this->insert('city',array(
            'name' => 'Калуга',          
        ));
        $this->insert('city',array(
            'name' => 'Каменск-Уральский',          
        ));
        $this->insert('city',array(
            'name' => 'Каменск-Шахтинский',          
        ));
        $this->insert('city',array(
            'name' => 'Камышин',          
        ));
        $this->insert('city',array(
            'name' => 'Канск',          
        ));
        $this->insert('city',array(
            'name' => 'Каспийск',          
        ));

        $this->insert('city',array(
            'name' => 'Керчь',          
        ));
        $this->insert('city',array(
            'name' => 'Кинешма',          
        ));
        $this->insert('city',array(
            'name' => 'Кириши',          
        ));
        $this->insert('city',array(
            'name' => 'Киров (Кировская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Кирово-Чепецк',          
        ));
        $this->insert('city',array(
            'name' => 'Киселевск',          
        ));
        $this->insert('city',array(
            'name' => 'Кисловодск',          
        ));
        $this->insert('city',array(
            'name' => 'Клин',          
        ));
        $this->insert('city',array(
            'name' => 'Клинцы',          
        ));
        $this->insert('city',array(
            'name' => 'Ковров',          
        ));
        $this->insert('city',array(
            'name' => 'Когалым',          
        ));
        $this->insert('city',array(
            'name' => 'Коломна',          
        ));
        $this->insert('city',array(
            'name' => 'Комсомольск-на-Амуре',          
        ));
        $this->insert('city',array(
            'name' => 'Копейск',          
        ));
        $this->insert('city',array(
            'name' => 'Королев',          
        ));
        $this->insert('city',array(
            'name' => 'Кострома',          
        ));

        $this->insert('city',array(
            'name' => 'Котлас',          
        ));
        $this->insert('city',array(
            'name' => 'Красногорск',          
        ));
        $this->insert('city',array(
            'name' => 'Краснодар',          
        ));
        $this->insert('city',array(
            'name' => 'Краснокаменск',          
        ));
        $this->insert('city',array(
            'name' => 'Краснокамск',          
        ));
        $this->insert('city',array(
            'name' => 'Краснотурьинск',          
        ));
        $this->insert('city',array(
            'name' => 'Красноярск',          
        ));
        $this->insert('city',array(
            'name' => 'Кропоткин',          
        ));
        $this->insert('city',array(
            'name' => 'Крымск',          
        ));
        $this->insert('city',array(
            'name' => 'Кстово',          
        ));
        $this->insert('city',array(
            'name' => 'Кузнецк',          
        ));
        $this->insert('city',array(
            'name' => 'Кумертау',          
        ));
        $this->insert('city',array(
            'name' => 'Кунгур',          
        ));
        $this->insert('city',array(
            'name' => 'Курган',          
        ));
        $this->insert('city',array(
            'name' => 'Курск',          
        ));
        $this->insert('city',array(
            'name' => 'Кызыл',          
        ));

        $this->insert('city',array(
            'name' => 'Лабинск',          
        ));
        $this->insert('city',array(
            'name' => 'Лениногорск',          
        ));
        $this->insert('city',array(
            'name' => 'Ленинск-Кузнецкий',          
        ));
        $this->insert('city',array(
            'name' => 'Лесосибирск',          
        ));
        $this->insert('city',array(
            'name' => 'Липецк',          
        ));
        $this->insert('city',array(
            'name' => 'Лиски',          
        ));
        $this->insert('city',array(
            'name' => 'Лысьва',          
        ));
        $this->insert('city',array(
            'name' => 'Лыткарино',          
        ));
        $this->insert('city',array(
            'name' => 'Люберцы',          
        ));
        $this->insert('city',array(
            'name' => 'Магадан',          
        ));
        $this->insert('city',array(
            'name' => 'Магнитогорск',          
        ));
        $this->insert('city',array(
            'name' => 'Майкоп',          
        ));
        $this->insert('city',array(
            'name' => 'Махачкала',          
        ));
        $this->insert('city',array(
            'name' => 'Междуреченск',          
        ));
        $this->insert('city',array(
            'name' => 'Мелеуз',          
        ));
        $this->insert('city',array(
            'name' => 'Миасс',          
        ));
        $this->insert('city',array(
            'name' => 'Минеральные Воды',          
        ));
        $this->insert('city',array(
            'name' => 'Минусинск',          
        ));
        $this->insert('city',array(
            'name' => 'Михайловка',          
        ));
        $this->insert('city',array(
            'name' => 'Михайловск (Ставропольский край)',          
        ));
        $this->insert('city',array(
            'name' => 'Мичуринск',          
        ));
        $this->insert('city',array(
            'name' => 'Москва',          
        ));
        $this->insert('city',array(
            'name' => 'Мурманск',          
        ));
        $this->insert('city',array(
            'name' => 'Муром',          
        ));
        $this->insert('city',array(
            'name' => 'Мытищи',          
        ));
        $this->insert('city',array(
            'name' => 'Набережные Челны',          
        ));
        $this->insert('city',array(
            'name' => 'Назарово',          
        ));
        $this->insert('city',array(
            'name' => 'Назрань',          
        ));
        $this->insert('city',array(
            'name' => 'Нальчик',          
        ));
        $this->insert('city',array(
            'name' => 'Наро-Фоминск',          
        ));
        $this->insert('city',array(
            'name' => 'Находка',          
        ));
        $this->insert('city',array(
            'name' => 'Невинномысск',          
        ));
        $this->insert('city',array(
            'name' => 'Нерюнгри',          
        ));
        $this->insert('city',array(
            'name' => 'Нефтекамск',          
        ));
        $this->insert('city',array(
            'name' => 'Нефтеюганск',          
        ));
        $this->insert('city',array(
            'name' => 'Нижневартовск',          
        ));
        $this->insert('city',array(
            'name' => 'Нижнекамск',          
        ));
        $this->insert('city',array(
            'name' => 'Нижний Тагил',          
        ));
        $this->insert('city',array(
            'name' => 'Новоалтайск',          
        ));
        $this->insert('city',array(
            'name' => 'Новокузнецк',          
        ));
        $this->insert('city',array(
            'name' => 'Новокуйбышевск',          
        ));
        $this->insert('city',array(
            'name' => 'Новомосковск',          
        ));
        $this->insert('city',array(
            'name' => 'Новосибирск',          
        ));
        $this->insert('city',array(
            'name' => 'Новотроицк',          
        ));
        $this->insert('city',array(
            'name' => 'Новоуральск',          
        ));
        $this->insert('city',array(
            'name' => 'Новочебоксарск',          
        ));
        $this->insert('city',array(
            'name' => 'Новочеркасск',          
        ));
        $this->insert('city',array(
            'name' => 'Новошахтинск',          
        ));
        $this->insert('city',array(
            'name' => 'Новый Уренгой',          
        ));
        $this->insert('city',array(
            'name' => 'Ногинск',          
        ));
        $this->insert('city',array(
            'name' => 'Норильск',          
        ));
        $this->insert('city',array(
            'name' => 'Ноябрьск',          
        ));
        $this->insert('city',array(
            'name' => 'Нягань',          
        ));
        $this->insert('city',array(
            'name' => 'Обнинск',          
        ));
        $this->insert('city',array(
            'name' => 'Одинцово',          
        ));
        $this->insert('city',array(
            'name' => 'Озерск (Челябинская область)',          
        ));
        $this->insert('city',array(
            'name' => 'Октябрьский',          
        ));
        $this->insert('city',array(
            'name' => 'Омск',          
        ));
        $this->insert('city',array(
            'name' => 'Орел',          
        ));
        $this->insert('city',array(
            'name' => 'Оренбург',          
        ));
        $this->insert('city',array(
            'name' => 'Орехово-Зуево',          
        ));
        $this->insert('city',array(
            'name' => 'Орск',          
        ));
        $this->insert('city',array(
            'name' => 'Павлово',          
        ));
        $this->insert('city',array(
            'name' => 'Павловский Посад',          
        ));
        $this->insert('city',array(
            'name' => 'Пенза',          
        ));
        $this->insert('city',array(
            'name' => 'Первоуральск',          
        ));
        $this->insert('city',array(
            'name' => 'Пермь',          
        ));
        $this->insert('city',array(
            'name' => 'Петрозаводск',          
        ));
        $this->insert('city',array(
            'name' => 'Петропавловск-Камчатский',          
        ));
        $this->insert('city',array(
            'name' => 'Подольск',          
        ));
        $this->insert('city',array(
            'name' => 'Полевской',          
        ));
        $this->insert('city',array(
            'name' => 'Прокопьевск',          
        ));
        $this->insert('city',array(
            'name' => 'Прохладный',          
        ));
        $this->insert('city',array(
            'name' => 'Псков',          
        ));
        $this->insert('city',array(
            'name' => 'Пушкино',          
        ));
        $this->insert('city',array(
            'name' => 'Пятигорск',          
        ));
        $this->insert('city',array(
            'name' => 'Раменское',          
        ));
        $this->insert('city',array(
            'name' => 'Ревда',          
        ));
        $this->insert('city',array(
            'name' => 'Реутов',          
        ));
        $this->insert('city',array(
            'name' => 'Ржев',          
        ));
        $this->insert('city',array(
            'name' => 'Рославль',          
        ));
        $this->insert('city',array(
            'name' => 'Россошь',          
        ));
        $this->insert('city',array(
            'name' => 'Ростов-на-Дону',          
        ));
        $this->insert('city',array(
            'name' => 'Рубцовск',          
        ));
        $this->insert('city',array(
            'name' => 'Рыбинск',          
        ));
        $this->insert('city',array(
            'name' => 'Рязань',          
        ));
        $this->insert('city',array(
            'name' => 'Салават',          
        ));
        $this->insert('city',array(
            'name' => 'Сальск',          
        ));
        $this->insert('city',array(
            'name' => 'Самара',          
        ));
        $this->insert('city',array(
            'name' => 'Санкт-Петербург',          
        ));
        $this->insert('city',array(
            'name' => 'Саранск',          
        ));
        $this->insert('city',array(
            'name' => 'Сарапул',          
        ));
        $this->insert('city',array(
            'name' => 'Саратов',          
        ));
        $this->insert('city',array(
            'name' => 'Саров',          
        ));
        $this->insert('city',array(
            'name' => 'Свободный',          
        ));
        $this->insert('city',array(
            'name' => 'Севастополь',          
        ));
        $this->insert('city',array(
            'name' => 'Северодвинск',          
        ));
        $this->insert('city',array(
            'name' => 'Северск',          
        ));
        $this->insert('city',array(
            'name' => 'Сергиев Посад',          
        ));
        $this->insert('city',array(
            'name' => 'Серов',          
        ));
        $this->insert('city',array(
            'name' => 'Серпухов',          
        ));
        $this->insert('city',array(
            'name' => 'Сертолово',          
        ));
        $this->insert('city',array(
            'name' => 'Сибай',          
        ));
        $this->insert('city',array(
            'name' => 'Симферополь',          
        ));
        $this->insert('city',array(
            'name' => 'Славянск-на-Кубани',          
        ));
        $this->insert('city',array(
            'name' => 'Смоленск',          
        ));
        $this->insert('city',array(
            'name' => 'Соликамск',          
        ));
        $this->insert('city',array(
            'name' => 'Солнечногорск',          
        ));
        $this->insert('city',array(
            'name' => 'Сосновый Бор',          
        ));
        $this->insert('city',array(
            'name' => 'Ставрополь',          
        ));
        $this->insert('city',array(
            'name' => 'Старый Оскол',          
        ));
        $this->insert('city',array(
            'name' => 'Стерлитамак',          
        ));
        $this->insert('city',array(
            'name' => 'Ступино',          
        ));
        $this->insert('city',array(
            'name' => 'Сургут',          
        ));
        $this->insert('city',array(
            'name' => 'Сызрань',          
        ));
        $this->insert('city',array(
            'name' => 'Сыктывкар',          
        ));
        $this->insert('city',array(
            'name' => 'Таганрог',          
        ));
        $this->insert('city',array(
            'name' => 'Тамбов',          
        ));
        $this->insert('city',array(
            'name' => 'Тверь',          
        ));
        $this->insert('city',array(
            'name' => 'Тимашевск',          
        ));
        $this->insert('city',array(
            'name' => 'Тихвин',          
        ));
        $this->insert('city',array(
            'name' => 'Тихорецк',          
        ));
        $this->insert('city',array(
            'name' => 'Тобольск',          
        ));
        $this->insert('city',array(
            'name' => 'Тольятти',          
        ));
        $this->insert('city',array(
            'name' => 'Томск',          
        ));
        $this->insert('city',array(
            'name' => 'Троицк',          
        ));
        $this->insert('city',array(
            'name' => 'Туапсе',          
        ));
        $this->insert('city',array(
            'name' => 'Туймазы',          
        ));
        $this->insert('city',array(
            'name' => 'Тула',          
        ));
        $this->insert('city',array(
            'name' => 'Тюмень',          
        ));
        $this->insert('city',array(
            'name' => 'Узловая',          
        ));
        $this->insert('city',array(
            'name' => 'Улан-Удэ',          
        ));
        $this->insert('city',array(
            'name' => 'Ульяновск',          
        ));
        $this->insert('city',array(
            'name' => 'Урус-Мартан',          
        ));
        $this->insert('city',array(
            'name' => 'Усолье-Сибирское',          
        ));
        $this->insert('city',array(
            'name' => 'Уссурийск',          
        ));
        $this->insert('city',array(
            'name' => 'Усть-Илимск',          
        ));
        $this->insert('city',array(
            'name' => 'Уфа',          
        ));
        $this->insert('city',array(
            'name' => 'Ухта',          
        ));
        $this->insert('city',array(
            'name' => 'Феодосия',          
        ));
        $this->insert('city',array(
            'name' => 'Фрязино',          
        ));
        $this->insert('city',array(
            'name' => 'Хабаровск',          
        ));
        $this->insert('city',array(
            'name' => 'Ханты-Мансийск',          
        ));
        $this->insert('city',array(
            'name' => 'Хасавюрт',          
        ));
        $this->insert('city',array(
            'name' => 'Химки',          
        ));
        $this->insert('city',array(
            'name' => 'Чайковский',          
        ));
        $this->insert('city',array(
            'name' => 'Чапаевск',          
        ));
        $this->insert('city',array(
            'name' => 'Чебоксары',          
        ));
        $this->insert('city',array(
            'name' => 'Челябинск',          
        ));
        $this->insert('city',array(
            'name' => 'Черемхово',          
        ));
        $this->insert('city',array(
            'name' => 'Череповец',          
        ));
        $this->insert('city',array(
            'name' => 'Черкесск',          
        ));
        $this->insert('city',array(
            'name' => 'Черногорск',          
        ));
        $this->insert('city',array(
            'name' => 'Чехов',          
        ));
        $this->insert('city',array(
            'name' => 'Чистополь',          
        ));
        $this->insert('city',array(
            'name' => 'Чита',          
        ));
        $this->insert('city',array(
            'name' => 'Шадринск',          
        ));
        $this->insert('city',array(
            'name' => 'Шали',          
        ));
        $this->insert('city',array(
            'name' => 'Шахты',          
        ));
        $this->insert('city',array(
            'name' => 'Шуя',          
        ));
        $this->insert('city',array(
            'name' => 'Щекино',          
        ));
        $this->insert('city',array(
            'name' => 'Щелково',          
        ));
        $this->insert('city',array(
            'name' => 'Электросталь',          
        ));
        $this->insert('city',array(
            'name' => 'Элиста',          
        ));
        $this->insert('city',array(
            'name' => 'Энгельс',          
        ));
        $this->insert('city',array(
            'name' => 'Южно-Сахалинск',          
        ));
        $this->insert('city',array(
            'name' => 'Юрга',          
        ));
        $this->insert('city',array(
            'name' => 'Якутск',          
        ));
        $this->insert('city',array(
            'name' => 'Ялта',          
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {

    }
}
