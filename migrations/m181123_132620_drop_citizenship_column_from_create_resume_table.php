<?php

use yii\db\Migration;

/**
 * Handles dropping citizenship from table `create_resume`.
 */
class m181123_132620_drop_citizenship_column_from_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('create_resume', 'citizenship');
        $this->addColumn('create_resume', 'citizenship', $this->integer());

        $this->createTable('citizen', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);

        $this->createIndex('idx-create_resume-citizenship', 'create_resume', 'citizenship', false);
        $this->addForeignKey("fk-create_resume-citizenship", "create_resume", "citizenship", "citizen", "id");

        $this->insert('citizen',array(
            'name' => 'Российская Федерация',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-create_resume-citizenship','create_resume');
        $this->dropIndex('idx-create_resume-citizenship','create_resume');
        
        $this->dropColumn('create_resume', 'citizenship');
        $this->addColumn('create_resume', 'citizenship', $this->string(255));


        $this->dropTable('citizen');
    }
}
