<?php

use yii\db\Migration;

/**
 * Handles adding telegram_name to table `create_resume`.
 */
class m181124_123304_add_telegram_name_column_to_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('create_resume', 'telegram_name', $this->string(255)->comment('Имя телеграма'));
        $this->addColumn('create_resume', 'telegram_id', $this->string(255)->comment('Ид телеграма'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('create_resume', 'telegram_name');
        $this->dropColumn('create_resume', 'telegram_id');
    }
}
