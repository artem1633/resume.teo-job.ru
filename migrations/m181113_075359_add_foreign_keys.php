<?php

use yii\db\Migration;

/**
 * Class m181113_075359_add_foreign_keys
 */
class m181113_075359_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-create_resume-city_id', 'create_resume', 'city_id', false);
        $this->addForeignKey("fk-create_resume-city_id", "create_resume", "city_id", "city", "id");

        $this->createIndex('idx-experience-create_id', 'experience', 'create_id', false);
        $this->addForeignKey("fk-experience-create_id", "experience", "create_id", "create_resume", "id");

        $this->createIndex('idx-education-create_id', 'education', 'create_id', false);
        $this->addForeignKey("fk-education-create_id", "education", "create_id", "create_resume", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-create_resume-city_id','create_resume');
        $this->dropIndex('idx-create_resume-city_id','create_resume');

        $this->dropForeignKey('fk-experience-create_id','experience');
        $this->dropIndex('idx-experience-create_id','experience');

        $this->dropForeignKey('fk-education-create_id','education');
        $this->dropIndex('idx-education-create_id','education');
    }
}
