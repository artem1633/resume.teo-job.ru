<?php

use yii\db\Migration;

/**
 * Handles dropping citizen from table `alerts`.
 */
class m181124_140535_drop_citizen_column_from_alerts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('alerts', 'citizen');
        $this->addColumn('alerts', 'citizen', $this->integer()->comment('Гражданство'));

        $this->createIndex('idx-alerts-citizen', 'alerts', 'citizen', false);
        $this->addForeignKey("fk-alerts-citizen", "alerts", "citizen", "citizen", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-alerts-citizen','alerts');
        $this->dropIndex('idx-alerts-citizen','alerts');

        $this->dropColumn('alerts', 'citizen');
        $this->addColumn('alerts', 'citizen', $this->string(255));
    }
}
