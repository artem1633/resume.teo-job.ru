<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m181113_074023_create_city_table extends Migration
{
    /** 
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
        $this->insert('city',array(
            'name' => 'Казань',          
        ));
        $this->insert('city',array(
            'name' => 'Сочи',          
        ));
        $this->insert('city',array(
            'name' => 'Нижний Новгород',          
        ));
        $this->insert('city',array(
            'name' => 'Ярославль',          
        ));
        $this->insert('city',array(
            'name' => 'Калининград',          
        ));
        $this->insert('city',array(
            'name' => 'Екатеринбург',          
        ));
        $this->insert('city',array(
            'name' => 'Анапа',          
        ));
        $this->insert('city',array(
            'name' => 'Владимир',          
        ));
        $this->insert('city',array(
            'name' => 'Новороссийск ',          
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('city');
    }
}
