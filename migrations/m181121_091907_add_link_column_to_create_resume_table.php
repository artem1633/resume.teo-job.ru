<?php

use yii\db\Migration;

/**
 * Handles adding link to table `create_resume`.
 */
class m181121_091907_add_link_column_to_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('create_resume', 'link', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('create_resume', 'link');
    }
}
