<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m181113_092949_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'value' => $this->text(),
        ]);

        $this->insert('settings',array(
            'name' => 'Уведомления о новом заполнение (галочка)',          
            'key' => 'notifications_new_filling',
            'value' => '',
        ));
        $this->insert('settings',array(
            'name' => 'Уведомления о Переходе',          
            'key' => 'transition_notifications',
            'value' => '',
        ));
        $this->insert('settings',array(
            'name' => 'Поле для ввод телеграмм ключа',          
            'key' => 'key_entry_field',
            'value' => '',
        ));
        $this->insert('settings',array(
            'name' => 'Поле для редактирования текста который показываеть для преглошения в телеграмм',          
            'key' => 'telegram_editing',
            'value' => '',
        ));
        $this->insert('settings',array(
            'name' => 'Поле для прокси',          
            'key' => 'proxy_field',
            'value' => '',
        ));
        $this->insert('settings',array(
            'name' => 'html текст политики конфендециальности',          
            'key' => 'html_text_privacy_policy',
            'value' => '',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
