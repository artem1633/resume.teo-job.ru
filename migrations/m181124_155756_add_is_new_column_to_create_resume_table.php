<?php

use yii\db\Migration;

/**
 * Handles adding is_new to table `create_resume`.
 */
class m181124_155756_add_is_new_column_to_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('create_resume', 'is_new', $this->boolean()->comment('Новая'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('create_resume', 'is_new');
    }
}
