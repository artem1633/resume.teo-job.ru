<?php

use yii\db\Migration;

/**
 * Handles the creation of table `alerts`.
 */
class m181122_160459_create_alerts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('alerts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
            'city' => $this->text()->comment('Город'),
            'employment' => $this->integer()->comment('Занятость'),
            'schedule' => $this->integer()->comment('График работ'),
            'salary' => $this->float()->comment('Зарплата'),
            'citizen' => $this->string(255)->comment('Гражданство'),
            'prava_m' => $this->boolean()->comment('M'),
            'prava_a' => $this->boolean()->comment('A'),
            'prava_b' => $this->boolean()->comment('B'),
            'prava_c' => $this->boolean()->comment('C'),
            'prava_d' => $this->boolean()->comment('D'),
            'prava_tm' => $this->boolean()->comment('TM'),
            'prava_tv' => $this->boolean()->comment('TB'),
            'medical_book' => $this->boolean()->comment('Наличие мед книжки'),
            'permission' => $this->string(255)->comment('Должность'),
            'description' => $this->text()->comment('Описание'),
            'status' => $this->boolean()->comment('Статус'),
            'ordered' => $this->string(255)->comment('Заказчик'),
            'link' => $this->string(255)->comment('Ссылка на тест'),
            'date_cr' => $this->datetime()->comment('Дата и время создания'),
            'date_send' => $this->datetime()->comment('Дата и время отправки'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('alerts');
    }
}
