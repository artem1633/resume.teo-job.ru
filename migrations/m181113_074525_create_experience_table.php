<?php

use yii\db\Migration;

/**
 * Handles the creation of table `experience`.
 */
class m181113_074525_create_experience_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('experience', [
            'id' => $this->primaryKey(),
            'organization' => $this->string(255),
            'position' => $this->string(255),
            'duties_achievements' => $this->text(),
            'create_id' => $this->integer(),
            'date_with' => $this->date(),
            'date_by' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('experience');
    }
}
