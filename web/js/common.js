$(document).ready(function() {
    $('.hamburger').on('click', function(){
        $('header').toggleClass('active');
    })
    
     $(window).on('click', function(e){
        if (e.target.classList.contains('hdr_menu')) {
            $('header').removeClass('active');
        }
    })

     $('header .dropdown a.open_toggle').on('click', function(e){
     	e.preventDefault();
        if($(this).next().is(':visible')){
            $('header ul.dropdown_body').slideUp();
        }
        else{
            $('header ul.dropdown_body').slideUp();
     	    $(this).next().slideToggle();
        }
     })
});