<?php

namespace app\controllers;

use Yii;
use app\models\CreateResume;
use app\modules\api\controllers\BotinfoController;
use app\models\CreateResumeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\mpdf\Pdf;
use app\models\EducationSearch;
use app\models\ExperienceSearch;
use app\models\Education;
use app\models\Experience;
use yii\web\UploadedFile;
use app\models\Chat;
/**
 * CreateResumeController implements the CRUD actions for CreateResume model.
 */
class CreateResumeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CreateResume models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreateResumeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintlist($id)
    {
        $fio= CreateResume::find()->where(['id'=>$id])->one();
        $searchModel = new CreateResumeSearch();
        $dataProvider = $searchModel->searchByCreate(Yii::$app->request->queryParams,$id);
        $dataProvider->pagination = ['pageSize' => 1000,];
        $educationsearchModel = new EducationSearch();
        $educationdataProvider = $educationsearchModel->searchByEducation(Yii::$app->request->queryParams,$id);
        $experiencesearchModel = new ExperienceSearch();
        $experiencedataProvider = $experiencesearchModel->searchByExperience(Yii::$app->request->queryParams,$id);
        $experiencedataProvider = $experiencedataProvider->getModels();
        $educationdataProvider = $educationdataProvider->getModels();
        $dataProvider = $dataProvider->getModels();
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $content = $this->renderPartial('@app/views/create-resume/print_create',[
            'dataProvider' => $dataProvider,
            'educationdataProvider'=> $educationdataProvider,
            'experiencedataProvider'=> $experiencedataProvider,
        ]);

        $pdf = new Pdf([
            //'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>['TEO-JOB.RU'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        if($type == 2)
        {
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->SetHeader($fio->name.''); // call methods or set any properties
            $mpdf->WriteHtml($content); // call mpdf write html
            echo $mpdf->Output('file.pdf', 'D'); // call the mpdf api output as needed
        }
        return $pdf->render();
    }
    /**
     * Displays a single CreateResume model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $education = Education::find()->where(['create_id'=>$id])->all();
        $experience = Experience::find()->where(['create_id'=>$id])->all();

        $chat = new Chat();
        $chat->chat_id = '#resume-'.$id;
        $model = $this->findModel($id);
        $model->is_new = 0;
        $model->save();

        if ( $request->post() ) {
            if($request->post()['text'] != ''){
                $chat->text = $request->post()['text'];
                $chat->save();
                if ($model->connect_telegram) {
                    BotinfoController::getReq('sendMessage', ['chat_id' => $model->telegram_id, 'parse_mode'=>'HTML', 'text' => $request->post()['text']]);
                }

            }
        }

        $chatText = Chat::find()->where(['chat_id' => '#resume-'.$id ])->all();

        return $this->render('view', [
            'model' => $model,
            'education'=>$education,
            'experience'=>$experience,
            'chatText' => $chatText,
        ]);
    }
    /**
     * Updates an existing CreateResume model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if(!empty($model->file))
            {
                $model->file->saveAs('uploads/'.$model->file->baseName.'.'.$model->file->extension);
                $model->image = $model->file->baseName.'.'.$model->file->extension;
                Yii::$app->db->createCommand()->update('create_resume', ['image' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
            }
            $educationall = Education::find()->where(['create_id'=> $id])->all();
            foreach ($educationall as $value) {
                if (($rel = Education::findOne($value->id)) !== null) {
                    $rel->delete();
                }
            }
            $model->education = $post['CreateResume']['education'];

            foreach ($model->education as $value) {
                $education = new Education();
                $education->create_id=$model->id;
                $education->educational_institution = $value['educational'];
                $education->faculty = $value['faculty'];
                $education->specialty = $value['specialty'];
                $education->form_study = $value['form_study'];
                $education->year_ending = $value['year_ending'];
                $education->save();
            }
            $experienceall = Experience::find()->where(['create_id'=> $id])->all();
            foreach ($experienceall as $value) {
                if (($rel = Experience::findOne($value->id)) !== null) {
                    $rel->delete();
                }
            }
            $model->experience = $post['CreateResume']['experience'];

            foreach ($model->experience as $value) {
                if($value['organization'] != null || $value['position'] != null || $value['duties_achievements'] != null)
                {
                    $experience = new Experience();
                    $experience->create_id=$model->id;
                    $experience->organization = $value['organization'];
                    $experience->position = $value['position'];
                    $experience->date_with = $value['date_with'];
                    $experience->date_by = $value['date_by'];
                    $experience->duties_achievements = $value['duties_achievements'];
                    $experience->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id ]);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete an existing CreateResume model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $education = Education::find()->where(['create_id' => $id])->all();
        foreach ($education as $educations) {
            $educations->delete();
        }
        $experiences = Experience::find()->where(['create_id' => $id])->all();
        foreach ($experiences as $experience) {
            $experience->delete();
        }
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        return $this->redirect(['index']);

    }

    /**
     * Delete multiple existing CreateResume model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the CreateResume model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CreateResume the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CreateResume::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
