<?php

namespace app\controllers;


class AdminController extends \yii\web\Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->redirect(['/users/dashboard']);
    }

    public function actionCity()
    {
        return $this->redirect(['/city/index']);
    }

}
