<?php

namespace app\modules\admin\controllers;
use yii\web\NotFoundHttpException;
use Yii;
use yii\helpers\Url;
use yii\web\Controller; 

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$request = Yii::$app->request;
        if (!isset($_GET['ssilka'])) throw new NotFoundHttpException('The requested page does not exist.');
        $questionary = CreateResume::find()->where(['ssilka' => $_GET['ssilka'] ])->one();
        return $this->render('home/create-resume/view',['id'=>$questionary->id]);
    }
}
