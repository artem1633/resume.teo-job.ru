<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreateResume */
?>
<div class="create-resume-update">

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
