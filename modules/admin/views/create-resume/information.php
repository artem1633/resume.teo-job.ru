<?php

use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\models\Settings;
/* @var $this yii\web\View */
/* @var $model app\models\FmStructure */
$settings = Settings::find()->where(['key'=>'html_text_privacy_policy'])->one();
$this->title = ' ';
?>
<div class="box box-default ">
    <div class="box-body"> 
       <div class="row">
            <div class="col-md-12 resume-container" style="margin-top: 10px"><br>
            	<?=Html::decode($settings->value)?>
            </div>
       </div>
    </div>
</div>
