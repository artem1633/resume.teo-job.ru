<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Education */
?>
<div class="education-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'educational_institution',
            'faculty',
            'specialty',
            'form_study',
            'year_ending',
        ],
    ]) ?>

</div>
