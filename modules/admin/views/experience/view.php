<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Experience */
?>
<div class="experience-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'organization',
            'position',
            'duties_achievements:ntext',
            'date_with',
            'date_by',
        ],
    ]) ?>

</div>
