<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Experience */
?>
<div class="experience-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
