<?php

use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\models\Education;
use yii\bootstrap\Modal; 
use app\models\Experience; 
use yii\widgets\ActiveForm;
use app\models\Settings;
/* @var $this yii\web\View */ 
/* @var $model app\models\CreateResume */
\johnitvn\ajaxcrud\CrudAsset::register($this);
$education = Education::find()->where(['create_id'=>$model->id])->all();
$experience = Experience::find()->where(['create_id'=>$model->id])->all();
$session = Yii::$app->session;

$setting = Settings::find()->where(['key' => 'text_after_filling'])->one();
$text = str_replace ("{unique_code_for_telegram}", $model->link , $setting->value);

if (!file_exists('uploads/'.$model->image) || $model->image == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/nouser.png';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/'.$model->image;
}
?>
<div id="w3-success-0" class="alert-success alert fade in">

<?= $text ?>

</div>
<?php $form = ActiveForm::begin(); ?>
<span  style="margin-left: 355px;">
    <?= isset($session['resumeId']) ? Html::a('Редактировать', ['/home/create-resume/update', /*'id' => $model->id*/], ['title'=> 'Редактировать', 'style' => 'background:#1b78b6;color:#ffffff;margin-top: 20px', 'class'=>'btn']) : '' ?>
    <?= Html::a('Скачать в pdf', ['/home/create-resume/print', 'id' => $model->id], ['title'=> 'Скачать в pdf', 'style' => 'margin-top: 20px', 'class'=>'btn btn-default'])?>
    <?= Html::a('Распечатать', ['/home/create-resume/printlist', 'id' => $model->id], ['target' => '_blan','title'=> 'Распечатать', 'style' => 'margin-top: 20px', 'class'=>'btn btn-default'])?>
    <?= isset($session['resumeId']) ? Html::a('Удалить', ['/home/create-resume/delete','id'=>$model->id], ['class'=>'btn btn-danger', 'style' => 'margin-top: 20px','role'=>'modal-remote','title'=>'Удалить', 
        'data-confirm'=>false, 'data-method'=>false,
        'data-request-method'=>'post',
        'data-toggle'=>'tooltip',
        'data-confirm-title'=>'Подтвердите действие',
        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента ?']) : '' ?>
</span>
<div class="box box-default " style="margin-top: 30px;margin-bottom: 20px">
    <div class="box-body">
      
       <div class="row">

        <table class="resume-container" style="box-shadow: 0 0 15px rgba(0,0,0,0.3);width: 700px;margin-left: 220px">
            <tr>
            <td>
            <div class="col-md-12">
                <br>
                <h1 style="color: #1b78b6"><b><?=Html::encode($model->surname.' '.$model->name.' '.$model->middle_name)?></b></h1>
                <div class="table-responsive">
                    <div class="row">
                        <div class="col-md-6">
                            <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                                <tbody>
                                    <tr>
                                        <td ><?=$model->getAttributeLabel('position')?> : <b><?=Html::encode($model->position)?></b></td>
                                    </tr>
                                    <tr>
                                        <td><?=$model->getAttributeLabel('desired_salary')?> : <b><?=Html::encode($model->desired_salary)?></b></td>
                                    </tr>
                                </tbody>
                            </table><hr>
                            <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                                <tbody>
                                    <tr>
                                        <td ><?=$model->getAttributeLabel('email')?> : <b><?=Html::encode($model->email)?></b></td>
                                    </tr>
                                    <tr>
                                        <td ><?=$model->getAttributeLabel('phone')?> : <b><?=Html::encode($model->phone)?></b></td>
                                    </tr>
                                </tbody>
                            </table><hr>
                            <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                                <tbody>
                                    <tr>
                                        <td><?=$model->getAttributeLabel('travel')?> : <b><?=Html::encode($model->getGrids($model->travel))?></b></td>
                                    </tr>
                                    <tr>
                                        <td><?=$model->getAttributeLabel('employment')?> : <b><?=Html::encode($model->getEmployments($model->employment))?></b></td>
                                    </tr>
                                    <tr>
                                        <td><?=$model->getAttributeLabel('schedule')?> : <b><?=Html::encode($model->getSchedules($model->schedule))?></b></td>
                                    </tr>
                                    <tr>
                                        <td>Водительские права : <b><?=$model->getPrava()?></b></td>
                                    </tr>                                                        
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                                <tbody>
                                    <tr>
                                        <td ><?=Html::img('/uploads/'.$model->image,['style'=>'width:250px; height:220px'])?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><hr>
                    <h3 style="color: #1b78b6"><i class="fa fa-user"> </i> <b> Личная информация</b></h3>
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td ><?=$model->getAttributeLabel('city_id')?> : <b><?=Html::encode($model->city->name)?></b></td>
                            </tr>
                            <tr>
                                <td ><?=$model->getAttributeLabel('moving')?> : <b><?=Html::encode($model->getMovings($model->moving))?></b></td>
                            </tr>
                            <tr>
                                <td ><?=$model->getAttributeLabel('citizenship')?> : <b><?=Html::encode($model->citizenship0->name)?></b></td>
                            </tr>
                            <tr>
                                <td><?=$model->getAttributeLabel('floor')?> : <b><?=Html::encode($model->getFloors($model->floor))?></b></td>
                            </tr>
                            <tr>
                                <td ><?=$model->getAttributeLabel('familiy_status')?> : <b><?=Html::encode($model->getFamiliys($model->familiy_status))?></b></td>
                            </tr>
                            <tr>
                                <td><?=$model->getAttributeLabel('have_kids')?> : <b><?=Html::encode($model->getGrids($model->have_kids))?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
                    
                    <?php if(count($education) != 0) { ?><h3 style="color: #1b78b6"><i class="fa fa-mortar-board"> </i> <b> Образование</b></h3> <?php } ?>
                    <?php  $number = 0; foreach ($education as $model1) {  $number++;?>
                        <div style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                            <p>Учебное заведение : <b><?=Html::encode($model1->educational_institution)?></b></p>
                            <p>Факультет : <b><?=Html::encode($model1->faculty)?></b></p>
                            <p>Форма обучения : <b><?=Html::encode($model1->getSpecialty($model1->specialty))?></b></p>
                            <p>Специальность : <b><?=Html::encode($model1->form_study)?></b></p>
                            <p>Год окончания : <b><?=Html::encode($model1->year_ending)?></b><p>
                        </div>
                    <?php }?>
                    <br>
                    <?php if(count($experience) != 0) { ?><h3 style="color: #1b78b6"><i class="fa fa-briefcase"> </i> <b> Опыт работы</b></h3> <?php } ?>
                    <?php  $number = 0; foreach ($experience as $model1) {  $number++;?>
                        <div style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                            <p >Организация : <b><?=Html::encode($model1->organization)?></b></p>
                            <p >Должность : <b><?=Html::encode($model1->position)?></b></p>
                            <p >Период работы c : <b><?=Html::encode($model1->date_with)?></b></p>
                            <p >Период работы по : <b><?=Html::encode($model1->date_by)?></b></p>
                            <p >Обязанности и достижения : <b><?=Html::encode($model1->duties_achievements)?></b></p>
                        </div>
                        <hr>
                    <?php }?>
                    <br>
                </div>
                <h3><i class="fa fa-files-o"> </i> <b> Ваша ссылка :  <?= Html::a('<span class="is-hidden-mobile">http://'. $_SERVER['SERVER_NAME'].'/'.$model->ssilka .'  </span>', ['/'.$model->ssilka], ['data-pjax' => '0','target'=> "_blank"]) ?></b></h3><br>
            </div>
            </td>
            </tr>
            </table>
        </div>        
    </div>
</div>



<?php ActiveForm::end(); ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>