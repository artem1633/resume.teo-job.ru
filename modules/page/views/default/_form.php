<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
/* @var $this yii\web\View */
/* @var $model app\models\CreateResume */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="wrap">
<div class="box box-default resume-container" style="margin-top: 10px">
    <div class="box-body"> 
       <div class="row">
            <div class="col-md-12 col-xs-12"><br>
                <h1>Создать резюме</h1>
                <div class="row" style="margin-top: 20px">
                <div class="col-md-6 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-xs-4" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('surname')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'surname')->textInput(['maxlength' => true])->label(""); ?>         
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-4" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('name')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(""); ?>        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-4" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('middle_name')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true])->label(""); ?>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-4" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('position')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'position')->textInput(['maxlength' => true])->label(""); ?>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-8 col-xs-12" style="margin-top: 10px;">
                        <?= $form->field($model, 'travel')->checkbox()->label(""); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('desired_salary')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'desired_salary')->textInput(['type'=>'number','placeholder' => '1000'])->label(""); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('email')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder' => 'example@example.ru'])->label(""); ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('phone')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                         <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '+7 (999) 999-99-99','options' => ['placeholder' => '+7 (951) 000-00-00','class'=>'form-control',]])->label(""); ?> 
                    </div>
                </div>
                <div class="row " style="margin-top: 20px">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-2 col-xs-3">
                        <?= $form->field($model, 'viber')->checkbox()->label(""); ?>
                    </div>
                    <div class="col-md-3 col-xs-5" >
                        <?= $form->field($model, 'whatsapp')->checkbox()->label(""); ?>
                    </div>
                    <div class="col-md-3 col-xs-4">
                        <?= $form->field($model, 'telegram')->checkbox()->label(""); ?>
                    </div>
                </div>
                </div>
                <div class="col-md-6 col-xs-12">
                <div class="row">
                    <div class="col-md-8">
                        <div id="images-to-upload">
                            <?= $model->image != null ? '<img style="width:100%;" src="http://' . $_SERVER["SERVER_NAME"] . "/uploads/" . $model->image .' ">' : '' ?>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                            <?= $form->field($model, 'file')->fileInput(['class'=>"btn_image"]); ?>                
                    </div>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('employment')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'employment')->dropDownList($model->getEmployment(),[])->label(""); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                        <b><?=$model->getAttributeLabel('schedule')?></b>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <?= $form->field($model, 'schedule')->dropDownList($model->getSchedule(),[])->label(""); ?>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-12">
                <h1>Личная информация</h1>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('city_id')?></b>  
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <?= $form->field($model, 'city_id')->widget(kartik\select2\Select2::classname(), [
                                    'data' => $model->getCitys(),
                                    'size' => Select2::MEDIUM,
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'allowClear' => true,
                                    ],
                                ])->label("");?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('citizenship')?></b>  
                            </div>
                            <div class="col-md-8 col-xs-12">  
                                <?= $form->field($model, 'citizenship')->widget(kartik\select2\Select2::classname(), [
                                    'data' => $model->getCitizenList(),
                                    'theme' => Select2::THEME_CLASSIC,
                                    'options' => [/*'multiple' => true,'placeholder' => 'Выберите'*/],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'tags' => true,
                                        //'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 1000,
                                        
                                    ],
                                ])->label("");?>      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('birthday')?></b>  
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <?= $form->field($model, 'birthday')->textInput(['maxlength' => true,'type'=>'date'])->label(""); ?> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-5 col-xs-4" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('moving')?></b>  
                            </div>
                            <div class="col-md-7 col-xs-8">
                                <?= $form->field($model, 'moving')->dropDownList($model->getMoving(),[])->label(""); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-4" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('floor')?></b>  
                            </div>
                            <div class="col-md-7 col-xs-8">
                                <?= $form->field($model, 'floor')->dropDownList($model->getFloor(),[])->label(""); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-12" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('familiy_status')?></b>  
                            </div>
                            <div class="col-md-7 col-xs-12">
                                <?= $form->field($model, 'familiy_status')->dropDownList($model->getFamiliy(),[])->label(""); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-7 col-xs-12" style="margin-top: 10px;">
                                <?= $form->field($model, 'have_kids')->checkbox()->label(""); ?>
                            </div>
                        </div>
                    </div>
                </div><hr>
            </div>
                
                <div class="row">
                    <h1>Образование</h1>
                    <div class="col-md-12">
                        <?php echo $form->field($model, 'education')->widget(MultipleInput::className(), [
                                'class' =>'resume-container',
                                'columns' => [
                                    [
                                        'name'  => 'educational',
                                        'title' => 'Учебное заведение',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],
                                    [
                                        'name'  => 'faculty',
                                        'title' => 'Факультет',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],   
                                    [
                                        'name'  => 'specialty',
                                        'title' => 'Форма обучения',
                                        'type'  => 'dropDownList',
                                        'enableError' => true,
                                        'type'  => 'dropDownList',
                                        'items' => [
                                            1 => 'Очная',
                                            2 => 'Очно-заочная (вечерняя)',
                                            3 => 'Заочная',
                                            4 => 'Дистанционная',
                                        ],
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],   
                                    [
                                        'name'  => 'form_study',
                                        'title' => 'Специальность',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],   
                                    [
                                        'name'  => 'year_ending',
                                        'title' => 'Год окончания',
                                        'enableError' => true,

                                        'options' => [
                                            'type'=> 'date',
                                            'startView'=>'year',
                                            'minViewMode'=>'years',
                                            'format' => 'yyyy',
                                            'class' => 'input-priority',
                                        ]
                                    ],                     
                                ]
                            ])->label('');
                        ?>
                    </div>
                </div><hr>
                <div class="row">
                    <h1>Опыт работы</h1>
                    <div class="col-md-12">
                        <?php echo $form->field($model, 'experience')->widget(MultipleInput::className(), [
                                'class' =>'resume-container',
                                'columns' => [
                                    [
                                        'name'  => 'organization',
                                        'title' => 'Организация',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],
                                    [
                                        'name'  => 'position',
                                        'title' => 'Должность',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],   
                                    [
                                        'name'  => 'date_with',
                                        'title' => 'Период работы c',
                                        'enableError' => true,
                                        'options' => [
                                            'type'=>'date',
                                            'class' => 'input-priority',
                                        ]
                                    ], 
                                    [
                                        'name'  => 'date_by',
                                        'title' => 'Период работы по',
                                        'enableError' => true,
                                        'options' => [
                                            'type'=>'date',
                                           'class' => 'input-priority',
                                        ]
                                    ],   
                                    [
                                        'name'  => 'duties_achievements',
                                        'title' => 'Обязанности и достижения',
                                        'enableError' => true,
                                        'options' => [
                                            'class' => 'input-priority',
                                        ]
                                    ],                     
                                ]
                            ])->label('');
                        ?>
                    </div>
                </div>
            <div class="col-md-12 col-xs-12"><hr>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-3 col-xs-3" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('languages_id')?></b>  
                            </div>
                            <div class="col-md-9 col-xs-9">
                           
                                <?= $form->field($model, 'languages_id')->widget(kartik\select2\Select2::classname(), [
                                    'data' => $model->getLanguage(),
                                    'theme' => Select2::THEME_CLASSIC,
                                    'options' => ['multiple' => true,'placeholder' => 'Выберите'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'tags' => true,
                                        //'tokenSeparators' => [',', ' '],
                                        'maximumInputLength' => 1000,
                                        
                                    ],
                                ])->label("");?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12" style="margin-top: 10px;font-size: 14px">
                                <b><?= 'Водительские права'?></b>  
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_m')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_a')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_b')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_c')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_d')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_tm')->checkbox()->label(""); ?>
                            </div>
                            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                                <?= $form->field($model, 'prava_tv')->checkbox()->label(""); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-9 col-xs-12" style="margin-top: 10px;">
                                <?= $form->field($model, 'medical_book')->checkbox()->label(""); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12" style="margin-top: 20px;font-size: 14px">
                                <b><?=$model->getAttributeLabel('information')?></b>  
                            </div>
                            <div class="col-md-9 col-xs-12">
                                <?= $form->field($model, 'information')->textInput(['maxlength' => true])->label(""); ?>         
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-9 col-xs-12" style="margin-top: 10px;">
                                <?= $form->field($model, 'offers')->checkbox()->label(""); ?>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group" style="text-align: center;">
                        <?= Html::submitButton('Сохранить', ['class' =>'btn btn-success']) ?>
                    </div>
                <?php } ?>
            </div>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 10px">
        <p style="text-align: center;font-size: 14px">Нажимая "Cохранить" Вы соглашаетесь с <?= Html::a('Политика в отношении обработки персональных данных', ['/home/create-resume/information'],['target' => '_blan',])?></p>
    </div>
</div>
</div>
<?php ActiveForm::end(); ?>
<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var fileCollection = new Array();

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%;" src="'+e.target.result+'"> ';
                $('#images-to-upload').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload').append(template);
            };
        });
    });

});
JS
);
?>