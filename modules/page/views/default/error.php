<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Ошибка';
?>
<div class="row" >
    <div class="col-md-12">    
        <div class="site-error" style="padding-top: 150px; text-align: center;">

            <h1 class="text-danger"><?= Html::encode($this->title) ?></h1>

            <div class="alert alert-danger">
                Не найдено этот страница
            </div>

            <div style="text-align: center;">
                Если это ошибка сервера. <br>
                Свяжитесь с тех поддержкой.
            </div>

            <div>
                <br>
                <a href="/" class="btn btn-success">На главную</a>
            </div>
        </div>
    </div>
</div>
