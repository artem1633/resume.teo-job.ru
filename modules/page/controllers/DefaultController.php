<?php

namespace app\modules\page\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Questionary;
use app\models\Questions;
use app\models\Applications;
use app\models\Experience;
use app\models\additional\Contacts;
use yii\web\NotFoundHttpException;
use app\models\Education;
use app\models\CreateResume;
use yii\web\UploadedFile;
use app\models\Settings;

/**
 * Default controller for the `page` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (isset($_GET['link'])) {

            if($_GET['link'] == 'admin'){
                return $this->redirect(['/admin/index']);
            }

            $model = CreateResume::find()->where(['ssilka' => $_GET['link'] ])->one();
            if($model != null){
                return $this->render('view', [
                    'model' => $model,
                    'education'=>$education,
                    'experience'=>$experience,
                ]);
            }
            else{
                return $this->render('error', [
                    'model' => $model,
                ]);
            }
        }
        else{
            $model = new CreateResume();
            $post = $request->post();
            if ($model->load($request->post()) && $model->save()) {
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/'.$model->file->baseName.'.'.$model->file->extension);
                    $model->image = $model->file->baseName.'.'.$model->file->extension;
                    Yii::$app->db->createCommand()->update('create_resume', ['image' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }

                $model->education = $post['CreateResume']['education'];
                        
                foreach ($model->education as $value) {
                    $education = new Education();
                    $education->create_id=$model->id;
                    $education->educational_institution = $value['educational'];
                    $education->faculty = $value['faculty'];
                    $education->specialty = $value['specialty'];
                    $education->form_study = $value['form_study'];
                    $education->year_ending = $value['year_ending'];
                    $education->save();
                }
                $model->experience = $post['CreateResume']['experience'];
                        
                foreach ($model->experience as $value) {
                    if($value['organization'] != null || $value['position'] != null || $value['duties_achievements'] != null){
                        $experience = new Experience();
                        $experience->create_id=$model->id;
                        $experience->organization = $value['organization'];
                        $experience->position = $value['position'];
                        $experience->date_with = $value['date_with'];
                        $experience->date_by = $value['date_by'];
                        $experience->duties_achievements = $value['duties_achievements'];
                        $experience->save();
                    }
                }
                $result = '';
                $i = 0;
                foreach ($post['CreateResume']['languages_id'] as $value) {
                    if($i == 0) $result = $value;
                    else $result .= ',' . $value;
                    $i++;
                }
                $model->languages_id = $result;
                $model->save();

                $education = Education::find()->where(['create_id'=>$model->id])->all();
                $experience = Experience::find()->where(['create_id'=>$model->id])->all();

                /*Yii::$app->session->setFlash('success', "Успешно отправлено. <br>Если вы хотите получать предложения о работе, передите в наш телеграм бот и введите код “".$model->ssilka."” ");*/
                /*$setting = Settings::find()->where(['key' => 'text_after_filling'])->one();
                $text = str_replace ("{unique_code_for_telegram}", $model->link , $setting->value);
                Yii::$app->session->setFlash('success', "Успешно отправлено. <br> ".$text);*/
                $session = Yii::$app->session;
                $session['resumeId'] = $model->id;

                return $this->redirect(['/'. $model->ssilka]);                        
            }
            else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }            
        }        
    }
}
