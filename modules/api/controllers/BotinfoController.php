<?php

namespace app\modules\api\controllers;

use app\models\Alerts;
use app\models\City;
use app\models\CreateResume;
use app\models\Chat;
use app\models\Settings;
use app\models\Users;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        if ($text == '/start') {
            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>" {$name} Привет! Меня создали для того чтобы я информарировал тебя, о новых вакансиях
               Чтобы я начал работать введи свой код:",
            ]);

            return true;

        }


        if ($text != '/start') {

            $users = CreateResume::find()->where(['telegram_id' => $chat_id])->one();

            if (!$users) {
                $users = CreateResume::find()->where(['link' => $text])->one();
            }

            if (!$users) {
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=>"{$name}, мы не нашли твою анкету! \nЕсли ты еще не заполнял форму для подбора предложений то заполни ее https://resume.teo-job.ru/ .
                    \nЕсли ты заполнял то попробуй еще раз ввести пароль:",
                    //'parse_mode'=>'HTML',
                ]);

                $text = "Мы не нашли {$text} анкету пользователя {$users->surname} {$users->name}: @".$username;
                $this->sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);

                return true;
            } else {
                //работа с чатом начало
                if ($users->connect_telegram) {

                    $chat = new Chat();
                    $chat->chat_id = '#resume-'.(string)$users->id;
                    $chat->user_id = 1;
                    $chat->text = $text;
                    $chat->is_read = 0;
                    if ($chat->save()){
                        $users->is_new = 1;
                        if (!$users->save()) {
                            $a = serialize($chat->getErrors());
                            $this->sendadmin(['chat_id' => $chat_id, 'parse_mode'=>'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
                        } else {
                            $text = ' Новое сообщение {$text}';
                            if ($users->surname) $text .= "\n {$users->surname} {$users->name} \n https://resume.teo-job.ru/create-resume/view?id={$users->id}";
                            $this->sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);
                        }
                        return true;
                    } else {
                        $a = serialize($chat->getErrors());
                        $this->sendadmin(['chat_id' => $chat_id, 'parse_mode'=>'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
                        return true;
                    }

                    return true;
                    //работа с чатом конец
                } else {
                    $users->connect_telegram = 1;
                    $users->telegram_id = (string)$chat_id;
                    $users->telegram_name = (string)$username;
                    $users->is_new = 1;
                    if ($users->save()) {
                        $this->getReq('sendMessage',[
                            'chat_id'=> $chat_id,
                            'text'=>"Твое резюме найдино {$users->surname} {$users->name}. 
                            \nПоздравляю с успешным подключением. Скоро мы начнем отправлять предложения согласно анкете! Так же вы можете сюда писать вопросы администратору, или написать ему в личку @technology_to_everyone",
                            'parse_mode'=>'HTML',
                        ]);
                        $text = 'Новый пользователь в боте';
                        if ($users->surname) $text .= "\n {$users->surname} {$users->name}: @".$username;
                        $this->sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);

                        return true;
                    }
                }
            }
        }

        $a = serialize($users->getErrors());

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
            'parse_mode'=>'HTML',
        ]);

        $text = 'Что то пошло не так';
        $text .= "У пользователя {$users->surname} {$users->name}: @".$username."\n {$a}";
        $this->sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);

        //return true;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function Sendadmin($text)
    {

        $token = Settings::find()->where(['key' => 'key_entry_field'])->one()->value;
        $proxy = Settings::find()->where(['key' => 'proxy_field'])->one()->value;

        $url =  "https://api.telegram.org/bot{$token}/sendMessage"; //основная строка и метод
        if(count($text)){
            $url=$url.'?'.http_build_query($text);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }



    public function getReq($method,$params=[],$decoded=0){ //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $token = Settings::find()->where(['key' => 'key_entry_field'])->one()->value;
        $proxy = Settings::find()->where(['key' => 'proxy_field'])->one()->value;

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = Settings::find()->where(['key' => 'key_entry_field'])->one()->value;
        $proxy = Settings::find()->where(['key' => 'proxy_field'])->one()->value;

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://resume.teo-job.ru/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function actionInfoall()
    {
        $allpush = Alerts::find()->where(['status' => false])->all();
        /** @var Alerts $push */
        foreach ($allpush as $push) {
            $allResume = CreateResume::find()->where(['connect_telegram' => true])->all();

            if($push->city != null && !is_array($push->city)){
                $result = explode(',', $push->city);
            }
            $string = '';
            $i = 0;
            foreach ($result as $value) {
                $cityName = City::findOne($value)->name;
                if($i == 0) $string .= $cityName;
                else $string .= '; ' . $cityName;
                $i++;
            }

            $text = "Новое предложение:\n";
            if ($push->name) $text .= "Название: {$push->name}\n";
            if ($string) $text .= "Для городов: {$string}\n";
            if ($push->getEmploymentValue()) $text .= "Занятость:  {$push->getEmploymentValue()}\n";
            if ($push->getScheduleValue()) $text .= "График работ:  {$push->getScheduleValue()}\n";
            if ($push->permission) $text .= "Должность:  {$push->permission}:\n";
            if ($push->salary) $text .= "Зарплата:  {$push->salary}\n";
            if ($push->description) $text .= "Описание:  {$push->description}\n";

            /** @var CreateResume $resume */
            foreach ($allResume as $resume) {
                $this->sendadmin(['chat_id' => $resume->telegram_id, 'parse_mode'=>'HTML', 'text' => $text]);
            }
            $push->status = true;
            $push->citizen = (string)$push->citizen;
            if (!$push->save()) {
                echo serialize($push->getErrors());
            }
        }
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function actionTestpush()
    {
        $text = 'Проверка пуша';
        $this->sendadmin(['chat_id' => '247187885', 'parse_mode'=>'HTML', 'text' => $text]);
    }







}
