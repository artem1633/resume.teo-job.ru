<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "experience".
 *
 * @property int $id
 * @property string $organization
 * @property string $position
 * @property string $duties_achievements
 * @property string $date_with
 * @property string $date_by
 *
 * @property CreateResume[] $createResumes
 */
class Experience extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duties_achievements'], 'string'],
            [['date_with', 'date_by'], 'safe'],
            [['create_id'], 'integer'],
            [['organization', 'position'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization' => 'Организация',
            'position' => 'Должность',
            'duties_achievements' => 'Должностные обязанности и достижения',
            'date_with' => 'Период работы c',
            'date_by' => 'Период работы по',
            'create_id' => 'Создать резюме',
        ];
    } 
    public function beforeSave($insert)
    {
        if($this->date_with != null ) $this->date_with = \Yii::$app->formatter->asDate($this->date_with, 'php:Y-m-d');
        if($this->date_by != null ) $this->date_by = \Yii::$app->formatter->asDate($this->date_by, 'php:Y-m-d');
        return parent::beforeSave($insert);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreate()
    {
        return $this->hasOne(CreateResume::className(), ['id' => 'create_id']);
    }
}
