<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $name
 *
 * @property CreateResume[] $createResumes
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => ' Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateResumes()
    {
        return $this->hasMany(CreateResume::className(), ['languages_id' => 'id']);
    }
}
