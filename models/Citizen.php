<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citizen".
 *
 * @property int $id
 * @property string $name
 *
 * @property CreateResume[] $createResumes
 */
class Citizen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citizen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts()
    {
        return $this->hasMany(Alerts::className(), ['citizen' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateResumes()
    {
        return $this->hasMany(CreateResume::className(), ['citizenship' => 'id']);
    }
}
