<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "alerts".
 *
 * @property int $id
 * @property string $name Название
 * @property string $city Город
 * @property int $employment Занятость
 * @property int $schedule График работ
 * @property double $salary Зарплата
 * @property string $citizen Гражданство
 * @property int $prava_m M
 * @property int $prava_a A
 * @property int $prava_b B
 * @property int $prava_c C
 * @property int $prava_d D
 * @property int $prava_tm TM
 * @property int $prava_tv TB
 * @property int $medical_book Наличие мед книжки
 * @property string $permission Должность
 * @property string $description Описание
 * @property int $status Статус
 * @property string $ordered Заказчик
 * @property string $link Ссылка на тест
 * @property string $date_cr Дата и время создания
 * @property string $date_send Дата и время отправки
 */
class Alerts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alerts';
    }
    public $cities;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['city', 'description'], 'string'],
            [['employment', 'schedule', 'prava_m', 'prava_a', 'prava_b', 'prava_c', 'prava_d', 'prava_tm', 'prava_tv', 'medical_book', 'status'], 'integer'],
            [['salary'], 'number'],
            [['date_cr', 'date_send'], 'safe'],
            [['name', 'citizen', 'permission', 'ordered', 'link'], 'string', 'max' => 255],
            [['citizen'], 'exist', 'skipOnError' => true, 'targetClass' => Citizen::className(), 'targetAttribute' => ['citizen' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'city' => 'Город',
            'employment' => 'Занятость',
            'schedule' => 'График работ',
            'salary' => 'Зарплата',
            'citizen' => 'Гражданство',
            'prava_m' => 'M',
            'prava_a' => 'A',
            'prava_b' => 'B',
            'prava_c' => 'C',
            'prava_d' => 'D',
            'prava_tm' => 'TM',
            'prava_tv' => 'TB',
            'medical_book' => 'Наличие мед книжки',
            'permission' => 'Должность',
            'description' => 'Описание',
            'status' => 'Статус',
            'ordered' => 'Заказчик',
            'link' => 'Ссылка на тест',
            'date_cr' => 'Дата и время создания',
            'date_send' => 'Дата и время отправки',
            'cities' => 'Город',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            /*$length = 8;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string_two ='';
                for( $i = 0; $i < $length; $i++) {
                    $string_two .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( Alerts::find()->where(['link' => $string_two])->one() != null );

            $this->link = $string_two;*/

            $this->date_cr = date('Y-m-d H:i:s');
            $this->status = false;

        }
        return parent::beforeSave($insert);
    }

    public function getCitys()
    {
        return ArrayHelper::map(City::find()->all(), 'id', 'name');
    }

    public function getEmployment()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Полная',],
            ['id' => '2','type' => 'Частичная',],
            ['id' => '3','type' => 'Проектная',], 
            ['id' => '4','type' => 'Стажировка',],
            ['id' => '5','type' => 'Волонтёрство',],    
        ], 'id', 'type');
    }

    public function getSchedule()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Полный день',],
            ['id' => '2','type' => 'Сменный график',],
            ['id' => '3','type' => 'Гибкий график',], 
            ['id' => '4','type' => 'Удаленная работа',],
            ['id' => '5','type' => 'Вахтовый метод',],    
        ], 'id', 'type');
    }

    public function getEmploymentValue()
    {
        if($this->employment == 1) return 'Полная';
        if($this->employment == 2) return 'Частичная';
        if($this->employment == 3) return 'Проектная';
        if($this->employment == 4) return 'Стажировка';
        if($this->employment == 5) return 'Волонтёрство';
    }

    public function getScheduleValue()
    {
        if($this->employment == 1) return 'Полный день';
        if($this->employment == 2) return 'Сменный график';
        if($this->employment == 3) return 'Гибкий график';
        if($this->employment == 4) return 'Удаленная работа';
        if($this->employment == 5) return 'Вахтовый метод';
    }

    public function getCitizenList()
    {
        return ArrayHelper::map(Citizen::find()->all(), 'id', 'name');
    }

    public function getDescription($id)
    {
        if($id == 1) return 'Есть';
        else return 'Нет';
    }

    public function getStatusDescription()
    {
        if($this->status == 1) return 'Да';
        else return 'Нет';
    }

    public function getStatusList()
    {
        return [
            0 => 'Нет',
            1 => 'Да',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitizen0()
    {
        return $this->hasOne(Citizen::className(), ['id' => 'citizen']);
    }
}
