<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Experience;

/**
 * ExperienceSearch represents the model behind the search form about `app\models\Experience`.
 */
class ExperienceSearch extends Experience
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','create_id'], 'integer'],
            [['organization', 'position', 'duties_achievements', 'date_with', 'date_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Experience::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_id' => $this->create_id,
            'date_with' => $this->date_with,
            'date_by' => $this->date_by,
        ]);

        $query->andFilterWhere(['like', 'organization', $this->organization])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'duties_achievements', $this->duties_achievements]);

        return $dataProvider;
    }
    public function searchByExperience($params,$id)
    {
        $query = Experience::find()->where(['create_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_id' => $this->create_id,
            'date_with' => $this->date_with,
            'date_by' => $this->date_by,
        ]);

        $query->andFilterWhere(['like', 'organization', $this->organization])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'duties_achievements', $this->duties_achievements]);

        return $dataProvider;
    }
}
