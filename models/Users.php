<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use app\models\Products;
use app\models\Applications;
use app\models\additional\Common;
use app\models\additional\Columns;
use app\models\additional\Tariffs;
use app\models\additional\Permissions;
use app\models\additional\LegalInformation;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $type
 * @property string $telegram_id
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;
    public $file;
    public $page;

    const SUPER_COMPANY = '0';
    const COMPANY = '1';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['fio', 'login', 'password'], 'required'],
            [['agree'], 'integer'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['fio', 'login', 'password', 'new_password', 'telegram_id', 'telephone', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'telegram_id' => 'Id чат телеграма',
            'new_password' => 'Новый пароль',
            'telephone' => 'Телефон',
            'foto' => 'Аватар',
            'file' => 'Аватар',
            'agree' => 'Я согласен с обработкой персональных данных'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            
            // $length = 20;
            // $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            // do {
            //     $string ='';
            //     for( $i = 0; $i < $length; $i++) {
            //         $string .= $chars[rand(0,strlen($chars)-1)];
            //     }
            // } while ( Users::find()->where(['utm' => $string])->one() != null );

            // $this->utm = $string;
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }


        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {        
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function setDefaultValues()
    {
        $notification = new Notification();
        $notification->name = 'При новом посещение';
        $notification->key = 'visiting';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'При новом заполнении';
        $notification->key = 'filling';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'Новости и акция';
        $notification->key = 'news_and_action';
        $notification->value = '';
        $notification->save();

        $notification = new Notification();
        $notification->name = 'Сменили группу';
        $notification->key = 'change_group';
        $notification->value = '';
        $notification->save();
    }

    //Получить список типов пользователя.
    public function getType()
    {
        return [
            self::SUPER_COMPANY => 'Супер компания',
            self::COMPANY => 'Компания',
        ];
    }

    //Получить описание типов пользователя.
    public function getTypeDescription()
    {
        switch ($this->type) {
            case 0: return "Супер компания";
            case 1: return "Компания";
            default: return "Неизвестно";
        }
    }

    //Список тарифов.
    public function getTariffList()
    {
        $tariffs = Tariffs::find()->all();
        return ArrayHelper::map( $tariffs , 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResumeStatuses()
    {
        return $this->hasMany(ResumeStatus::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVacancies()
    {
        return $this->hasMany(Vacancy::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionaries()
    {
        return $this->hasMany(Questionary::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['user_id' => 'id']);
    }

}
