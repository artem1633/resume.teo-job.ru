<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "education".
 *
 * @property int $id
 * @property string $educational_institution 
 * @property string $faculty
 * @property int $specialty
 * @property string $form_study
 * @property string $year_ending
 * 
 * @property CreateResume[] $createResumes
 */
class Education extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() 
    {
        return [
            [['specialty','create_id'], 'integer'],
            [['year_ending'], 'safe'],
            [['educational_institution', 'faculty', 'form_study'], 'string', 'max' => 255],
        ];
    }
 
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'educational_institution' => 'Учебное заведение',
            'faculty' => 'Факультет',
            'specialty' => 'Форма обучения',
            'form_study' => 'Специальность',
            'year_ending' => 'Год окончания ',
            'create_id' => 'Создать резюме',
        ];
    }
    public function beforeSave($insert)
    {
        if($this->year_ending != null ) $this->year_ending = \Yii::$app->formatter->asDate($this->year_ending, 'php:Y-m-d');
        return parent::beforeSave($insert);
    }
    public function getSpecialty($id)
    {
        if($id == 1) return 'Очная';
        if($id == 2) return 'Очно-заочная (вечерняя)';
        if($id == 3) return 'Заочная';
        if($id == 4) return 'Дистанционная';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreate()
    {
        return $this->hasOne(CreateResume::className(), ['id' => 'create_id']);
    }
}
