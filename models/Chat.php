<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property int $id
 * @property string $chat_id Чат ид
 * @property int $user_id Пользователь
 * @property string $text Текст
 * @property string $date_time Дата и время
 * @property int $is_read Читаль или нет
 *
 * @property Users $user
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_read'], 'integer'],
            [['text'], 'string'],
            [['date_time'], 'safe'],
            [['chat_id'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Чат ид',
            'user_id' => 'Пользователь',
            'text' => 'Текст',
            'date_time' => 'Дата и время',
            'is_read' => 'Читаль или нет',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
            $this->date_time = date('Y-m-d H:i:s');
            $this->is_read = false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
