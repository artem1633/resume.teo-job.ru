<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/** 
 * This is the model class for table "create_resume".
 *
 * @property int $id
 * @property string $surname
 * @property string $name
 * @property string $middle_name
 * @property int $travel 
 * @property int $desired_salary 
 * @property string $email
 * @property string $phone
 * @property int $viber
 * @property int $whatsapp
 * @property int $telegram
 * @property string $image
 * @property string $ssilka
 * @property int $employment
 * @property int $schedule
 * @property int $city_id
 * @property int $experience_id
 * @property int $education_id
 * @property string $citizenship
 * @property string $birthday
 * @property int $moving
 * @property int $floor 
 * @property int $familiy_status
 * @property int $have_kids
 * @property string $languages
 * @property int $prava_m
 * @property int $prava_a
 * @property int $prava_b
 * @property int $prava_c
 * @property int $prava_d
 * @property int $prava_tm
 * @property int $prava_tv
 * @property int $medical_book
 * @property string $information
 * @property int $offers
 *
 * @property City $city
 * @property Education $education
 * @property Experience $experience
 */
class CreateResume extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $files;
    public $tag;
    public $file;
    public $experience;
    public $education;
    public static function tableName()
    {
        return 'create_resume';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['travel', 'desired_salary', 'viber', 'whatsapp', 'telegram', 'employment', 'schedule', /*'city_id',*/ 'moving', 'floor', 'familiy_status', 'have_kids', 'prava_m', 'prava_a', 'prava_b', 'prava_c', 'prava_d', 'prava_tm', 'prava_tv', 'medical_book', 'offers', 'connect_telegram', 'is_new' /*, 'languages_id'*/], 'integer'],
            [['birthday', 'date_cr'], 'safe'], 
            [['information'], 'string'],
            [['surname', 'name','position', 'middle_name', 'link','email', 'phone', 'image', 'ssilka', 'telegram_name', 'telegram_id' /*'citizenship'*/], 'string', 'max' => 255],
            /*[['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],*/
            ['email','email'],
            [['link','ssilka'], 'unique'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['surname','name','email','phone'], 'required'],
            ['city_id', 'validatCity'],
            ['citizenship', 'validateCitizen'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'file' => 'Фото',
            'middle_name' => 'Отчество',
            'position' => 'Должность',
            'travel' => 'Готовность к командировкам',
            'desired_salary' => 'Желаемая зарплата',
            'email' => 'Электронная почта',
            'phone' => 'Телефон для связи',
            'viber' => 'Viber',
            'whatsapp' => 'Whatsapp',
            'telegram' => 'Telegram',
            'image' => 'Фото',
            'ssilka' => 'Короткая ссылка',
            'employment' => 'Занятость',
            'schedule' => 'График работы',
            'city_id' => 'Город проживания',
            'citizenship' => 'Гражданство',
            'birthday' => 'Дата рождения',
            'moving' => 'Переезд', 
            'floor' => 'Пол',
            'familiy_status' => 'Семейное положение',
            'have_kids' => 'Есть дети',
            'languages_id' => 'Языки',
            'prava_m' => 'M',
            'prava_a' => 'A',
            'prava_b' => 'B',
            'prava_c' => 'C',
            'prava_d' => 'D',
            'prava_tm' => 'TM',
            'prava_tv' => 'TB',
            'prava_m1' => 'Водительские права M',
            'prava_a1' => 'Водительские права A',
            'prava_b1' => 'Водительские права B',
            'prava_c1' => 'Водительские права C',
            'prava_d1' => 'Водительские права D',
            'prava_tm1' => 'Водительские права TM',
            'prava_tv1' => 'Водительские права TB',
            'medical_book' => 'наличие медицинской книжки',
            'information' => 'Дополнительная информация',
            'offers' => 'в активном поиске, согласен чтобы работодатели мне звонили с предложениями',
            'connect_telegram' => 'Подключен к Телеграмм',
            'link' => 'Уникалный код',
            'date_cr' => 'Дата создание',
            'telegram_name' => 'Имя телеграма',
            'telegram_id' => 'Ид телеграма',
            'is_new' => 'Новая',
        ];
    }
     public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->connect_telegram = 0;
            $this->is_new = 1;
            $this->date_cr = date('Y-m-d H:i:s');
            $questionary = CreateResume::find()->one();
            $length = 8;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( CreateResume::find()->where(['ssilka' => $string])->one() != null );

            $this->ssilka = $string;

             do {
                $string_two ='';
                for( $i = 0; $i < $length; $i++) {
                    $string_two .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( CreateResume::find()->where(['link' => $string_two])->one() != null );

            $this->link = $string_two;

        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $education = Education::find()->where(['create_id' => $this->id])->all();
        foreach ($education as $value) {
            $value->delete();
        }

        $experience = Experience::find()->where(['create_id' => $this->id])->all();
        foreach ($experience as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    //Новая город
    public function validatCity($attribute, $params)
    {
        $city = City::find()->where(['id' => $this->city_id])->one();
        if (!isset($city))
        {
            $city = new City();
            $city->name = $this->city_id;
            
            if ($city->save())
            {
                $this->city_id = $city->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новый город");
            }
        }
    }

    //Новая Гражданство
    public function validateCitizen($attribute, $params)
    {
        $citizen = Citizen::find()->where(['id' => $this->citizenship])->one();
        if (!isset($citizen))
        {
            $citizen = new Citizen();
            $citizen->name = $this->citizenship;
            
            if ($citizen->save())
            {
                $this->citizenship = $citizen->id;
            }
            else
            {
                $this->addError($attribute,"Не создано новое гражданство");
            }
        }
    }

    public function getEmployment()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Полная',],
            ['id' => '2','type' => 'Частичная',],
            ['id' => '3','type' => 'Проектная',], 
            ['id' => '4','type' => 'Стажировка',],
            ['id' => '5','type' => 'Волонтёрство',],    
        ], 'id', 'type');
    }
    public function getEmployments($id)
    {
        if($id == 1) return 'Полная';
        if($id == 2) return 'Частичная';
        if($id == 3) return 'Проектная';
        if($id == 4) return 'Стажировка';
        if($id == 5) return 'Волонтёрство';
    }
    public function getSchedule()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Полный день',],
            ['id' => '2','type' => 'Сменный график',],
            ['id' => '3','type' => 'Гибкий график',], 
            ['id' => '4','type' => 'Удаленная работа',],
            ['id' => '5','type' => 'Вахтовый метод',],    
        ], 'id', 'type');
    }
    public function getSchedules($id)
    {
        if($id == 1) return 'Полный день';
        if($id == 2) return 'Сменный график';
        if($id == 3) return 'Гибкий график';
        if($id == 4) return 'Удаленная работа';
        if($id == 5) return 'Вахтовый метод';
    }

    public function getPrava()
    {
        $string = '';
        if($this->prava_m) $string .='M;';
        if($this->prava_a) $string .='A;';
        if($this->prava_b) $string .='B;';
        if($this->prava_c) $string .='C;';
        if($this->prava_d) $string .='D;';
        if($this->prava_tm) $string .='TM;';
        if($this->prava_tv) $string .='TB;';
        return $string;
    }

    public function getTelegram()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }

    public function getTelegramDescription()
    {
        if($this->connect_telegram == 1) return 'Да';
        else return 'Нет';
    }

    public function getMoving()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Невозможен',],
            ['id' => '2','type' => 'Возможен',],
            ['id' => '3','type' => 'Нежелателен',], 
            ['id' => '4','type' => 'Желателен',],
        ], 'id', 'type');
    }
    public function getMovings($id)
    {
        if($id == 1) return 'Невозможен';
        if($id == 2) return 'Возможен';
        if($id == 3) return 'Нежелателен';
        if($id == 4) return 'Желателен';
    }
    public function getFloor()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Мужской',],
            ['id' => '2','type' => 'Женский',],
        ], 'id', 'type');
    }
    public function getFloors($id)
    {
        if($id == 1) return 'Мужской';
        if($id == 2) return 'Женский';
    }
    public function getFamiliy()
    {
        return ArrayHelper::map([
            ['id' => '1','type' => 'Холост / Не замужем',],
            ['id' => '2','type' => 'Женат / Замужем',],
        ], 'id', 'type');
    }
    public function getFamiliys($id)
    {
        if($id == 1) return 'Холост / Не замужем';
        if($id == 2) return 'Женат / Замужем';
    }
    public function getGrids($id)
    {
        if($id == 0) return 'Нет';
        if($id == 1) return 'Да';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitizenship0()
    {
        return $this->hasOne(Citizen::className(), ['id' => 'citizenship']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
    public function getCitys()
    {
        return ArrayHelper::map(City::find()->all(), 'id', 'name');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducations()
    {
        return $this->hasMany(Education::className(), ['create_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['create_id' => 'id']);
    }
    public function getLanguages()
    {
        return $this->hasOne(Languages::className(), ['id' => 'languages_id']);
    }
    public function getLanguage()
    {
        return ArrayHelper::map(Languages::find()->all(), 'id', 'name');
    }
    public function getCitizenList()
    {
        return ArrayHelper::map(Citizen::find()->all(), 'id', 'name');
    }
}
