<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alerts;

/**
 * AlertsSearch represents the model behind the search form about `app\models\Alerts`.
 */
class AlertsSearch extends Alerts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employment', 'schedule'], 'integer'],
            [['name', 'city', 'citizen', 'prava_m', 'prava_a', 'prava_b', 'prava_c', 'prava_d', 'prava_tm', 'prava_tv', 'medical_book', 'permission', 'description', 'status', 'ordered', 'link', 'date_cr', 'date_send'], 'safe'],
            [['salary'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alerts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'employment' => $this->employment,
            'schedule' => $this->schedule,
            'salary' => $this->salary,
            'date_cr' => $this->date_cr,
            'date_send' => $this->date_send,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'citizen', $this->citizen])
            ->andFilterWhere(['like', 'prava_m', $this->prava_m])
            ->andFilterWhere(['like', 'prava_a', $this->prava_a])
            ->andFilterWhere(['like', 'prava_b', $this->prava_b])
            ->andFilterWhere(['like', 'prava_c', $this->prava_c])
            ->andFilterWhere(['like', 'prava_d', $this->prava_d])
            ->andFilterWhere(['like', 'prava_tm', $this->prava_tm])
            ->andFilterWhere(['like', 'prava_tv', $this->prava_tv])
            ->andFilterWhere(['like', 'medical_book', $this->medical_book])
            ->andFilterWhere(['like', 'permission', $this->permission])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'ordered', $this->ordered])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
