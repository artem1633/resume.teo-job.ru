<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CreateResume;

/**
 * CreateResumeSearch represents the model behind the search form about `app\models\CreateResume`.
 */
class CreateResumeSearch extends CreateResume
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'desired_salary', 'employment', 'schedule', 'city_id', 'moving', 'floor', 'familiy_status', 'languages_id', 'connect_telegram', 'is_new'], 'integer'],
            [['surname', 'name','position', 'middle_name', 'travel', 'email', 'phone', 'viber', 'whatsapp', 'telegram', 'image', 'ssilka', 'citizenship', 'birthday', 'have_kids', 'prava_m', 'prava_a', 'prava_b', 'prava_c', 'prava_d', 'prava_tm','link', 'prava_tv', 'medical_book', 'information', 'offers', 'date_cr', 'telegram_name', 'telegram_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CreateResume::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'desired_salary' => $this->desired_salary,
            'employment' => $this->employment,
            'schedule' => $this->schedule,
            'city_id' => $this->city_id,
            'birthday' => $this->birthday,
            'moving' => $this->moving,
            'floor' => $this->floor,
            'familiy_status' => $this->familiy_status,
            'connect_telegram' => $this->connect_telegram,
            'date_cr' => $this->date_cr,
            'is_new' => $this->is_new,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'travel', $this->travel])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'viber', $this->viber])
            ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
            ->andFilterWhere(['like', 'telegram', $this->telegram])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'ssilka', $this->ssilka])
            ->andFilterWhere(['like', 'citizenship', $this->citizenship])
            ->andFilterWhere(['like', 'have_kids', $this->have_kids])
            ->andFilterWhere(['like', 'languages', $this->languages])
            ->andFilterWhere(['like', 'prava_m', $this->prava_m])
            ->andFilterWhere(['like', 'prava_a', $this->prava_a])
            ->andFilterWhere(['like', 'prava_b', $this->prava_b])
            ->andFilterWhere(['like', 'prava_c', $this->prava_c])
            ->andFilterWhere(['like', 'prava_d', $this->prava_d])
            ->andFilterWhere(['like', 'prava_tm', $this->prava_tm])
            ->andFilterWhere(['like', 'prava_tv', $this->prava_tv])
            ->andFilterWhere(['like', 'medical_book', $this->medical_book])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'offers', $this->offers]);

        return $dataProvider;
    }
    public function searchByCreate($id)
    {
        $query = CreateResume::find()->where(['id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'desired_salary' => $this->desired_salary,
            'employment' => $this->employment,
            'schedule' => $this->schedule,
            'city_id' => $this->city_id,
            'birthday' => $this->birthday,
            'moving' => $this->moving,
            'floor' => $this->floor,
            'familiy_status' => $this->familiy_status,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'travel', $this->travel])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'viber', $this->viber])
            ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
            ->andFilterWhere(['like', 'telegram', $this->telegram])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'ssilka', $this->ssilka])
            ->andFilterWhere(['like', 'citizenship', $this->citizenship])
            ->andFilterWhere(['like', 'have_kids', $this->have_kids])
            ->andFilterWhere(['like', 'languages', $this->languages])
            ->andFilterWhere(['like', 'prava_m', $this->prava_m])
            ->andFilterWhere(['like', 'prava_a', $this->prava_a])
            ->andFilterWhere(['like', 'prava_b', $this->prava_b])
            ->andFilterWhere(['like', 'prava_c', $this->prava_c])
            ->andFilterWhere(['like', 'prava_d', $this->prava_d])
            ->andFilterWhere(['like', 'prava_tm', $this->prava_tm])
            ->andFilterWhere(['like', 'prava_tv', $this->prava_tv])
            ->andFilterWhere(['like', 'medical_book', $this->medical_book])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'offers', $this->offers]);

        return $dataProvider;
    }
}
