<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div><br>
<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none;width: 400px'>
 
 <?php
 $table = '';
 $number = 0; 

 foreach($dataProvider as $model)
 {
	 $number++;
	$table .= "
	<tr >
	 	<td width=700 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:32px;color: #1b78b6'>".'<b>'.$model->surname.' '.$model->name.' '.$model->middle_name.'</b>'."</span></p>
		</td><br>
	</tr>
	";
	$table .= "
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Должность : '.'<b>'.$model->position.'</b>'."</span></p>
		</td>
	 </tr>
	 
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Желаемая зарплата : '.'<b>'.$model->desired_salary.'</b>'."</span></p>
		</td>
	 </tr>
	<hr style='border-style: dashed;'>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Электронная почта : '.'<b>'.$model->email.'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Телефон для связи : '.'<b>'.$model->phone.'</b>'."</span></p>
		</td>
	 </tr><hr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Готовность к командировкам : '.'<b>'.$model->getGrids($model->travel).'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Занятость : '.'<b>'.$model->getEmployments($model->employment).'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'График работы : '.'<b>'.$model->getSchedules($model->schedule).'</b>'."</span></p>
		</td>
	 </tr>	 
    <tr>
        <td width=400 height=25>&nbsp;&nbsp;Водительские права : <b>".$model->getPrava()."</b></td>
    </tr>
	";
 } 
 echo $table;
 
 ?>
 
</table>
<table class=MsoNormalTable border= cellspacing=0 cellpadding=0
 style='margin-left:420px;margin-top:-202px ;border-collapse:collapse;border:none;'>
 
 <?php
 $table = '';
 $number = 0; 

 foreach($dataProvider as $model)
 {
 	if($model->image!=null){ 
	$table .= "
	 <tr >
	 	<td width=250 height=220>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'<b>'.Html::img('/uploads/'.$model->image,['style'=>'width:250px; height:220px']).'</b>'."</span></p>
		</td>
	 </tr>
	";
	}
 } 
 echo $table;
 
 ?>
 
</table>
<hr>
</div>
<div>
<h3 style="color: #1b78b6"><i class="fa fa-user"> </i> <b> Личная информация</b></h3>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='margin-left:.25pt;border-collapse:collapse;border:none'>
 <?php
 $table = '';
 $number = 0;

 foreach($dataProvider as $model)
 {
	 $number++;

	$table .= "

	<tr >
	 	<td width=360 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Город проживания : '.'<b>'.$model->city->name.'</b>'."</span></p>
		</td>
	</tr>
	<tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Гражданство : '.'<b>'.$model->name.'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Дата рождения : '.'<b>'.$model->birthday.'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Переезд : '.'<b>'.$model->getMovings($model->moving).'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Пол : '.'<b>'.$model->getFloors($model->floor).'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Семейное положение : '.'<b>'.$model->getFamiliys($model->familiy_status).'</b>'."</span></p>
		</td>
	 </tr>
	 <tr >
	 	<td width=400 height=25>
			<p class=MsoNormal align=center ><span 
			style='font-size:15px;'>".'&nbsp;&nbsp;'.'Есть дети : '.'<b>'.$model->getGrids($model->have_kids).'</b>'."</span></p>
		</td>
	 </tr>
	 
	";
 } 
 echo $table;
 
 ?>
</table><hr>
</div>

<div>
<?php if($count1!=0){ ?>
<h3 style="color: #1b78b6"><i class="fa fa-mortar-board"> </i> <b> Образование</b></h3>

 <?php
 $table = '';
 $number = 0;

 foreach($educationdataProvider as $model)
 {
	 $number++;
	$table .= "
	<div >
        <p>Учебное заведение : <b> " .$model->educational_institution . "</b></p>
        <p>Факультет : <b> " . $model->faculty . "</b></p>
        <p>Форма обучения : <b> " .$model->getSpecialty($model->specialty) . "</b></p>
        <p>Специальность : <b> " .$model->form_study . "</b></p>
        <p>Год окончания : <b> " . $model->year_ending . "</b><p>
    </div>	 
	";
 } 
 echo $table;
 
 ?>
 
<hr>
<?php }?>
</div>

<div>
<?php if($count2!=0){ ?>
<h3 style="color: #1b78b6"><i class="fa fa-briefcase"> </i> <b> Опыт работы</b></h3>

 <?php
 $table = '';
 $number = 0;

 foreach($experiencedataProvider as $model)
 {
	 $number++;
	$table .= "
	<div >
        <p >Организация : <b> ".$model->organization ."</b></p>
        <p >Должность : <b> " .$model->position ."</b></p>
        <p >Период работы c : <b> " .$model->date_with ."</b></p>
        <p >Период работы по : <b> " .$model->date_by ."</b></p>
        <p >Обязанности и достижения : <b>" .$model->duties_achievements ."</b></p>
    </div>
	 
	";
 } 
 echo $table;
 
 ?>
<hr>
<?php }?>
</div>
