<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CreateResume */

?>
<div class="create-resume-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
