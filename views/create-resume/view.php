<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use app\models\Languages;
use yii\widgets\ActiveForm;
use app\models\CreateResume;

/* @var $this yii\web\View */
/* @var $model app\models\CreateResume */
$language = '';
if($model->languages_id != null && !is_array($model->languages_id) ){
    $result  = explode(',', $model->languages_id );
}

foreach ($result as $value) {
    $language .= Languages::findOne($value)->name . '; ';
}
?>
<div class="create-resume-view">
    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title"><b>Резюме <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $model->id], ['title'=> 'Редактировать', 'class'=>'btn btn-warning btn-rounded'])?></b></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="content-frame-body content-frame-body-left" style="max-height: 470px; overflow: auto; " >
                        <div class="col-md-6">
                            <div style="margin: 10px;  overflow: auto;"> 
                                <p><b>Короткая ссылка : </b> <?= Html::a('<span class="is-hidden-mobile">http://'. $_SERVER['SERVER_NAME'].'/'.$model->ssilka .'  </span>', ['/'.$model->ssilka], ['data-pjax' => '0','target'=> "_blank"]) ?></p> 
                                <p><b>Фамилия</b> : <?=$model->surname?></p>
                                <p><b>Имя</b> : <?=$model->name?></p>
                                <p><b>Отчество</b> : <?=$model->middle_name?></p>
                                <p><b>Готовность к командировкам</b> : <?= $model->travel == 1 ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Желаемая зарплата</b> : <?= $model->desired_salary ?></p>
                                <p><b>Электронная почта</b> : <?= $model->email ?></p>
                                <p><b>Телефон для связи</b> : <?= $model->phone ?></p>
                                <p><b>Занятость</b> : <?= \yii\helpers\ArrayHelper::map([
                                                        ['id' => '1','type' => 'Полная',],
                                                        ['id' => '2','type' => 'Частичная',],
                                                        ['id' => '3','type' => 'Проектная',], 
                                                        ['id' => '4','type' => 'Стажировка',],
                                                        ['id' => '5','type' => 'Волонтёрство',],
                                                    ],'id','type')[$model->employment] ?></p>
                                <p><b>График работы</b> : <?= \yii\helpers\ArrayHelper::map([
                                                        ['id' => '1','type' => 'Полный день',],
                                                        ['id' => '2','type' => 'Сменный график',],
                                                        ['id' => '3','type' => 'Гибкий график',], 
                                                        ['id' => '4','type' => 'Удаленная работа',],
                                                        ['id' => '5','type' => 'Вахтовый метод',],    
                                                    ],'id','type')[$model->schedule] ?></p>
                                <p><b>Город проживания</b> : <?= $model->city->name ?></p>
                                <p><b>Гражданство</b> : <?= $model->citizenship ?></p>
                                <p><b>Пол</b> : <?= $model->floor == 1 ? 'Мужской' : 'Женский' ?></p>
                                <p><b>Семейное положение</b> : <?= $model->familiy_status == 1 ? 'Холост / Не замужем' : 'Женат / Замужем' ?></p>
                                <p><b>Есть дети</b> : <?= $model->have_kids ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права </b> : <?=$model->getPrava()?></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div style="margin: 10px;  overflow: auto;"> 
                                <p><b>Viber : </b> <?= $model->viber == 1 ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p> 
                                <p><b>Whatsapp</b> : <?= $model->whatsapp == 1 ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Telegram</b> : <?= $model->telegram == 1 ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Дата рождения</b> : <?= $model->birthday ?></p>
                                <p><b>Переезд</b> : <?=\yii\helpers\ArrayHelper::map([
                                        ['id' => '1','type' => 'Невозможен',],
                                        ['id' => '2','type' => 'Возможен',],
                                        ['id' => '3','type' => 'Нежелателен',], 
                                        ['id' => '4','type' => 'Желателен',],   
                                    ],'id','type')[$model->moving]?></p>
                                <p><b>Водительские права M</b> : <?= $model->prava_m ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права A</b> : <?= $model->prava_a ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права B</b> : <?= $model->prava_b ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права C</b> : <?= $model->prava_c ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права D</b> : <?= $model->prava_d ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p> 
                                <p><b>Водительские права TM</b> : <?= $model->prava_tm ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Водительские права TB </b> : <?= $model->prava_tv ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>наличие медицинской книжки</b> : <?= $model->medical_book ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>в активном поиске, согласен чтобы работодатели мне звонили с предложениями</b> : <?=$model->offers ? '<span style="color:green;" class="glyphicon glyphicon-ok-circle"></span>' : '<span style="color:red;" class="glyphicon glyphicon-remove-sign"></span>' ?></p>
                                <p><b>Языки</b> : <?= $language ?></p>
                                <p><b>Дополнительная информация</b> : <?= $model->information ?></p>
                            </div>
                        </div>
                                                            
                    </div>
                </div>
                <div class="panel-footer">
                </div>
            </div>  
        </div>
        <div class="col-md-5">
            <div class="col-md-12">

                            <div class="panel panel-warning">
                                <div class="panel-heading ui-draggable-handle">
                                    <h3 class="panel-title"><b>Чат</b></h3>
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="content-frame-body content-frame-body-left" style="max-height: 400px; overflow: auto; " >
                                        <div class="messages messages-img">
                                            <?php 
                                            foreach ($chatText as $value) {
                                                $identity = Yii::$app->user->identity;
                                                if($value->user_id == $identity->id) $in = '';
                                                else $in = 'in';
                                                if($value->user_id != null){
                                                    if (!file_exists('avatars/'.$identity->foto) || $identity->foto == '') {
                                                        $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
                                                    } else {
                                                        $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$identity->foto;
                                                    }
                                                }
                                                else{
                                                    $number = preg_replace("/[^0-9]{1,4}/", '', $value->chat_id);
                                                    $resume = CreateResume::findOne($number);
                                                    if($resume->image == null) $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/no-image.jpg';
                                                    else $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/' . $resume->image;
                                                }
                                            ?>
                                            <div class="item <?=$in?> item-visible">
                                                <div class="image">
                                                    <img src="<?=$path?>" alt="<?=$value->user->fio?>">
                                                </div>
                                                <div class="text">
                                                    <div class="heading">
                                                        <a href="#"><?=$value->user->fio?></a>
                                                        <span class="date"><?= date( 'H:i:s d.m.Y', strtotime($value->date_time) ) ?></span>
                                                    </div>
                                                    <?=$value->text?>
                                                </div>
                                            </div>
                                            <?php } ?>                            
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="panel panel-default push-up-10">
                                        <div class="panel-body panel-body-search">
                                            <?php $form = ActiveForm::begin(); ?>
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-warning"><span class="fa fa-camera"></span></button>
                                                        <button class="btn btn-danger"><span class="fa fa-chain"></span></button>
                                                    </div>
                                                    <input type="text" name="text" class="form-control" placeholder="Написать сообщение...">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-info">Отправить</button>
                                                    </div>
                                                </div>
                                            <?php ActiveForm::end(); ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
            <div class="col-md-12" style="padding-top: 30px;">
                <div class="panel panel-warning panel-toggled">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title"><b> </b></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content-frame-body content-frame-body-left" style="max-height: 350px; overflow: auto; " >
                            <div class="col-md-12">
                                <h3 style="color: #1b78b6"><i class="fa fa-mortar-board"> </i> <b> Образование</b></h3>
                                <div> 
                                    <table class="table" style="margin-top: 20px;">
                                        <tbody>
                                            <tr>
                                                <td >Учебное заведение</td>
                                                <td >Факультет</td>
                                                <td >Форма обучения</td>
                                                <td >Специальность</td>
                                                <td >Год окончания</td>
                                            </tr>
                                    <?php foreach ($education as $model1) { ?>
                                            <tr>
                                                <td ><b><?=Html::encode($model1->educational_institution)?></b></td>
                                                <td ><b><?=Html::encode($model1->faculty)?></b></td>
                                                <td ><b><?=Html::encode($model1->getSpecialty($model1->specialty))?></b></td>
                                                <td ><b><?=Html::encode($model1->form_study)?></b></td>
                                                <td ><b><?=Html::encode($model1->year_ending)?></b></td>
                                            </tr>
                                    <?php }?>
                                        </tbody>
                                    </table>
                                    <hr><hr>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h3 style="color: #1b78b6"><i class="fa fa-briefcase"> </i> <b> Опыт работы</b></h3>
                                <div style="margin: 10px; "> 
                                    <table class="table" style="margin-top: 20px;">
                                        <tbody>
                                            <tr>
                                                <td >Организация</td>
                                                <td >Должность</td>
                                                <td >Период работы c</td>
                                                <td >Период работы по</td>
                                                <td >Обязанности и достижения</td>
                                            </tr>
                                    <?php  $number = 0; foreach ($experience as $model1) { ?>
                                            <tr>
                                                <td ><b><?=Html::encode($model1->organization)?></b></td>
                                                <td ><b><?=Html::encode($model1->position)?></b></td>
                                                <td ><b><?=Html::encode($model1->date_with)?></b></td>
                                                <td ><b><?=Html::encode($model1->date_by)?></b></td>
                                                <td ><b><?=Html::encode($model1->duties_achievements)?></b></td>
                                            </tr>
                                    <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>                                                    
                            </div>
                        </div>

                    </div>

                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>      
    </div>
</div>
