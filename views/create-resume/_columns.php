<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\CreateResume;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
/*    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_cr',
    ],*/

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'surname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'middle_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'desired_salary',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'link',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telegram_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telegram_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_new',
        'filter' => CreateResume::getTelegram(),
        'content' => function($data){
            if($data->is_new == 1) return 'Да';
            if($data->is_new == 0) return 'Нет';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'connect_telegram',
        'filter' => CreateResume::getTelegram(),
        'content' => function($data){
            return $data->getTelegramDescription();
        }
    ],
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ssilka',
        'content' => function($data){
            return Html::a( 'http://' . $_SERVER['SERVER_NAME'] . '/' . $data->ssilka , [ '/'.$data->ssilka ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post', 
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   