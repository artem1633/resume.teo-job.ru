<?php

use yii\widgets\DetailView;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\CreateResume;
/* @var $this yii\web\View */
/* @var $model app\models\Education */
?>
<div class="education-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'educational_institution',
            'faculty',
            [
                'attribute'=>'specialty',
                'value'=> function($data){
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,'title'=> 'Очная'],
                        ['id' => 2,'title'=> 'Очно-заочная (вечерняя)'],
                        ['id' => 3,'title'=> 'Заочная'],
                        ['id' => 4,'title'=> 'Дистанционная'],
                    ],'id','title')[$data->specialty];
                }
            ],
            'form_study',
            'year_ending',
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=> 'create_id',
                'value'=> function($data){
                    return $data->create->name;
                }
            ],
        ],
    ]) ?>

</div>
