<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\CreateResume;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'educational_institution',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'faculty',
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'specialty',
        'filter' => array('1' => 'Очная' , '2' => 'Очно-заочная (вечерняя)', '3' => 'Заочная', '4' => 'Дистанционная'),
        'value'=> function($data){
            return \yii\helpers\ArrayHelper::map([
                ['id' => 1,'title'=> 'Очная'],
                ['id' => 2,'title'=> 'Очно-заочная (вечерняя)'],
                ['id' => 3,'title'=> 'Заочная'],
                ['id' => 4,'title'=> 'Дистанционная'],
            ],'id','title')[$data->specialty];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'form_study',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'year_ending',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=> 'create_id',
        'filter' => ArrayHelper::map(CreateResume::find()->all(),'id','name'),
        'content'=> function($data){
            return $data->create->name;
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   