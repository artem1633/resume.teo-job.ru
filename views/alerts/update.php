<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alerts */
?>
<div class="alerts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
