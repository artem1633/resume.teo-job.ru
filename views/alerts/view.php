<?php

use yii\widgets\DetailView;
use app\models\City;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alerts */
?>
<div class="alerts-view">
 <div class="panel panel-success panel-hidden-controls">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Оповещение</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>                                        
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'city',
                'value' => function($data){
                    if($data->city != null && !is_array($data->city)){
                        $result = explode(',', $data->city);
                    }
                    $string = '';
                    $i = 0;
                    foreach ($result as $value) {
                        $cityName = City::findOne($value)->name;
                        if($i == 0) $string .= $cityName;
                        else $string .= '; ' . $cityName;
                        $i++;
                    }
                    return $string;
                }
            ],
            [
                'attribute' => 'employment',
                'value' => $model->getEmploymentValue(),
            ],
            [
                'attribute' => 'schedule',
                'value' => $model->getScheduleValue(),
            ],
            'salary',
            [
                'attribute' => 'citizen',
                'value' => $model->citizen0->name,
            ],
            [
                'attribute' => 'prava_m',
                'value' => $model->getDescription($model->prava_m),
            ],
            [
                'attribute' => 'prava_a',
                'value' => $model->getDescription($model->prava_a),
            ],
            [
                'attribute' => 'prava_b',
                'value' => $model->getDescription($model->prava_b),
            ],
            [
                'attribute' => 'prava_c',
                'value' => $model->getDescription($model->prava_c),
            ],
            [
                'attribute' => 'prava_d',
                'value' => $model->getDescription($model->prava_d),
            ],
            [
                'attribute' => 'prava_tm',
                'value' => $model->getDescription($model->prava_tm),
            ],
            [
                'attribute' => 'prava_tv',
                'value' => $model->getDescription($model->prava_tv),
            ],
            [
                'attribute' => 'medical_book',
                'value' => $model->getDescription($model->medical_book),
            ],
            'permission',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => $model->getStatusDescription(),
            ],
            'ordered',
            'link',
            'date_cr',
            'date_send',
        ],
    ]) ?>
    </div>
    <div class="panel-footer">
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['data-pjax'=>'0','title'=> 'Изменить','class'=>'btn btn-info']) ?>
    </div>     
</div>
</div>
