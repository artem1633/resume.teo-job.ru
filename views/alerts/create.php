<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Alerts */

?>
<div class="alerts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
