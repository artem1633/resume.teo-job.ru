<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Alerts */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="panel panel-success panel-hidden-controls">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Оповещение</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>                                        
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cities')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getCitys(),
                    'pluginOptions' => [
                        'multiple' => true,
                        'allowClear' => true,
                    ],
                ]);?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'employment')->dropDownList($model->getEmployment(),[]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'schedule')->dropDownList($model->getSchedule(),[]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'salary')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'citizen')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getCitizenList(),
                    'theme' => Select2::THEME_CLASSIC,
                    'options' => [],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);?>  
            </div>            
        </div>

        <div class="row">
            <div class="col-md-2 col-xs-12" style="margin-top: 10px;font-size: 14px">
                <b><?= 'Водительские права'?></b>  
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_m')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_a')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_b')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_c')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_d')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_tm')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-1 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'prava_tv')->checkbox()->label(""); ?>
            </div>
            <div class="col-md-2 col-xs-3" style="margin-top: 12px;">
                <?= $form->field($model, 'medical_book')->checkbox()->label(""); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ordered')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'permission')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
            </div>
        </div>

    </div>      
    <div class="panel-footer">                                
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>
    </div>                            
</div>
<?php ActiveForm::end(); ?>