<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
?>
<div class="settings-view"> 
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'key',
            [
                'attribute'=>'value',
                'format'=>'html',   
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
                ],
            ],
        ],
    ]) ?>

</div>
