<?php
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\CreateResume;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'organization',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'position',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'duties_achievements',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_with',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_with, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=> 'create_id',
        'filter' => ArrayHelper::map(CreateResume::find()->all(),'id','name'),
        'content'=> function($data){
            return $data->create->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_by',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_by, 'php:d.m.Y');
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   