<?php

use yii\widgets\DetailView;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\CreateResume;
/* @var $this yii\web\View */
/* @var $model app\models\Experience */
?>
<div class="experience-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'organization',
            'position',
            'duties_achievements:ntext',
            'date_with',
            'date_by',
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=> 'create_id',
                'value'=> function($data){
                    return $data->create->name;
                }
            ],
        ],
    ]) ?>

</div>
