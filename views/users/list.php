<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;


$this->title = "Подключенные профили";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'list-pjax']) ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h4 class="panel-title">Подключенные профили</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/list_column.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить ещё один профиль <i class="fa fa-plus"></i>', ['add-new-profile'],
                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
