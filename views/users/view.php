<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

CrudAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'change-pjax']) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Личный кабинет</h3>
                    <span class="pull-right">
                        <a class="btn btn-warning btn-xs" title="Изменить" role="modal-remote" href="<?=Url::toRoute(['/users/change', 'id' => $model->id])?>"><i class="glyphicon glyphicon-pencil" ></i></a>
                    </span>
                </div>
                <div class="panel-body">
                    <div style="margin-top: 10px;" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2"><b><?=$model->getAttributeLabel('fio')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('login')?></b></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><?=$model->fio?></td>
                                    <td><?=$model->login?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('fio')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('telegram_id')?></b></td>
                                </tr>
                                <tr>
                                    <td><?=$model->fio?></td>
                                    <td><?=$model->telegram_id?></td>
                                    <td><?=$model->getTypeDescription()?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>