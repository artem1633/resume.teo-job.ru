<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Languages */
?>
<div class="languages-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
        ],
    ]) ?>

</div>
